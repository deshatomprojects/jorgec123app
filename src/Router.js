import React, {Component} from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import {logout} from './actions';
import {Colors} from './themes/';

// conatainers
import Home from './containers/Home';
import CreateGroup from './containers/CreateGroup';
import Profile from './containers/Profile';
import Login from './containers/Login';
import Group from './containers/Group';
import JoinGroup from './containers/JoinGroup';
import Register from './containers/Register';
import ManageUsers from './containers/ManageUsers';
import UnknownContainer from './containers/UnknownContainer';
import UnknownContainerTabs from './containers/UnknownContainerTabs';
import ManageReservation from './containers/ManageReservation';
import EditReservation from './containers/EditReservation';
import CardListContainer from './containers/CardListContainer';
import CardDetails from './containers/CardDetails';

// components
import {Button} from './components/common';
import IconInput from './components/IconInput';
import RightButton from './components/RightButton';
import SideMenuBar from './components/SideMenuBar';
import {
    Scene,
    Router,
    Modal,
    Schema,
    Actions,
    Reducer,
    ActionConst
} from 'react-native-router-flux';
const reducerCreate = params => {
    const defaultReducer = Reducer(params);
    return (state, action) => {
        console.log("ACTION:", action);
        return defaultReducer(state, action);
    }
};

// define this based on the styles/dimensions you use
const getSceneStyle = (/* NavigationSceneRendererProps */
props,
computedProps) => {
    const style = {
        flex: 1,
        backgroundColor: '#fff',
        shadowColor: null,
        shadowOffset: null,
        shadowOpacity: null,
        shadowRadius: null
    };
    if (computedProps.isActive) {
        style.marginTop = computedProps.hideNavBar
            ? 0
            : 64;
    }
    return style;
};

const SimpleButton =({onPress, title})=> {

  return (
      <TouchableOpacity onPress={onPress}>
        <Text style={{color:'white'}}>{title}</Text>
      </TouchableOpacity>
  );
};

let backButtonFunction = function() {

  return (
      <TouchableOpacity style={[{
        width: 100,
        height: 37,
        position: 'absolute',
        bottom: 4,
        left: 2,
        padding: 8,
        justifyContent:'center',
    }]} onPress={Actions.pop}>
        <View style={{flexDirection:'row', alignItems:'center'}}>
          <Icon name="ios-arrow-back" size={25} color={'#ffffff'} style={{marginTop:10,paddingRight:8}} />
        </View>
      </TouchableOpacity>
  );
};

class RouterComponent extends Component {
    constructor(props) {
        super(props);
    }

    renderHomeLeftButtons() {
        return (
            <JoinGroup></JoinGroup>
        )
    }

    render() {
        const {navigationBarStyle, titleStyle, sceneStyle} = styles;
        return (
            <Router createReducer={reducerCreate} panHandlers={null} getSceneStyle={getSceneStyle}>
                <Scene key="modal" component={Modal}>
                    <Scene key="root">
                        <Scene key="auth">
                            <Scene key="Login" component={Login} sceneStyle={sceneStyle} title="Login" hideNavBar initial/>
                            <Scene key="Register" component={Register} sceneStyle={sceneStyle} title="Register" hideNavBar/>
                        </Scene>
                        <Scene key="drawer" component={SideMenuBar}  >
                        <Scene key="main" direction="vertical" >
                            <Scene direction="vertical" key="Home" component={Home} sceneStyle={sceneStyle} title="My Groups" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} initial renderLeftButton={() => this.renderHomeLeftButtons()} renderRightButton={() => <RightButton onPress={() => Actions.CreateGroup()} iconName="md-add"/>}/>
                            <Scene key="Groups" renderBackButton={backButtonFunction} component={Group} sceneStyle={sceneStyle} title="Group (4301)" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} renderRightButton={() => <SimpleButton title={'Invite'}/>}/>
                            <Scene key="ManageUsers" renderBackButton={backButtonFunction} component={ManageUsers} sceneStyle={sceneStyle} title="Manage Users" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} />
                            <Scene key="ManageReservation" renderBackButton={backButtonFunction} component={ManageReservation} sceneStyle={sceneStyle} title="Manage Reservation"  renderRightButton={() => <SimpleButton title={'Add'}/>} navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} />
                            <Scene key="EditReservation" renderBackButton={backButtonFunction} component={EditReservation} sceneStyle={sceneStyle} title="Edit Reservation"  renderRightButton={() => <SimpleButton title={'Remove'}/>} navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} />
                            <Scene key="CreateGroup" renderBackButton={backButtonFunction} component={CreateGroup} sceneStyle={sceneStyle} title="Create Group" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle}/>
                            <Scene key="Profile" renderBackButton={backButtonFunction} component={Profile} sceneStyle={sceneStyle} title="EDIT PROFILE" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} rightTitle="logout" rightButtonTextStyle={{color: 'white'}} onRight={() => this.props.logout()}/>
                            <Scene key="UnknownContainer" renderBackButton={backButtonFunction} component={UnknownContainer} sceneStyle={sceneStyle} title="UnknownContainer" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} renderRightButton={() => <RightButton  iconName="md-images"/>} />
                            <Scene key="UnknownContainerTabs" renderBackButton={backButtonFunction} component={UnknownContainerTabs} sceneStyle={sceneStyle} title="Container Tabs" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} renderRightButton={() => <RightButton  iconName="md-images"/>} />
                            <Scene key="CardListContainer" renderBackButton={backButtonFunction} component={CardListContainer} sceneStyle={sceneStyle} title="Cart List Container" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} />
                            <Scene key="CardDetails" renderBackButton={backButtonFunction} component={CardDetails} sceneStyle={sceneStyle} title="Cart Details Container" navigationBarStyle={navigationBarStyle} titleStyle={titleStyle} />
                        </Scene>
                        </Scene>
                    </Scene>
                </Scene>
            </Router>
        );
    }
}

const styles = {
    navigationBarStyle: {
        backgroundColor: Colors.navBarColor,
        borderBottomColor: 'transparent'
    },
    titleStyle: {
        color: "#FFF"
    },
    sceneStyle: {
        flex: 1
    }
};



export default connect(null, {logout})(RouterComponent);
