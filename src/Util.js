export const updateField = ({
  prop,
  value,
  required,
  validate
}) => {

  let error = false;

  if (required) {
    if (!value || value === '') {
      error = true;
    }
  }

  if (validate) {
    if (validate === 'email') {
      let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      if (!re.test(value)) {
        error = true;
      }
    }
  }
  return error;
}
