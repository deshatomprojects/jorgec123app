import React, {Component} from 'react';
import Router from './Router';
import ReduxThunk from 'redux-thunk';
import axios from 'axios';
import {Actions} from 'react-native-router-flux';
import {Provider} from 'react-redux';
import {AsyncStorage} from 'react-native';
// import devToolsEnhancer from 'remote-redux-devtools';
import {persistStore, autoRehydrate} from 'redux-persist';
import {createStore, compose, applyMiddleware} from 'redux';
import {BASE_URL} from './constant';

import reducers from './reducers';

class App extends Component {
    componentWillMount() {
        axios.defaults.baseURL = BASE_URL;
        axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

    }

    render() {
        //realtime parameter to true or to other global variable to turn it off in production:
        const enhancer = compose(
          applyMiddleware(ReduxThunk),
          autoRehydrate(),
          //devToolsEnhancer({realtime: true})
        );
        const store = createStore(reducers, {}, enhancer);
        persistStore(store, {
            storage: AsyncStorage,
            blacklist: ['login']
        });
        //AsyncStorage.clear(); // <- Uncomment this to clean
        return (
            <Provider store={store}>
                <Router/>
            </Provider>
        );
    }
}
export default App;
