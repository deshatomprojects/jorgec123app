import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ListView,
  ScrollView,
  Dimensions
} from "react-native";
import { Button, H3 } from "../components/common";
import IconInput from "../components/IconInput";
import GooglePlaceInput from "../components/GooglePlaceInput";
import _ from "lodash";
import { AppStyles, Colors } from "../themes";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import ScrollableTabView from "react-native-scrollable-tab-view";
import ModalPicker from "react-native-modal-picker";
var { height, width } = Dimensions.get("window");
import ModalDropdown from "react-native-modal-dropdown";
import DatePicker from "react-native-datepicker";
import {updateUnknownField,mockRequestContainer} from '../actions';

class UnknownContainer extends Component {
  constructor(props) {
    super(props);
  }

  dropdownadjustFrame(frameStyle) {
    frameStyle.width = (width/2) - 12;
    return frameStyle;
  }


  renderDatePicker(stateName, format, model) {
    return (
      <DatePicker
        style={{
          borderColor: Colors.inputBlueBorderColor,
          width: width - 6,
          height: 34,
          marginTop: 0,
          marginBottom: 4
        }}
        date={this.props.unknown[stateName]}
        mode={model}
        placeholder="select date"
        format={format}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: "absolute",
            right: 0,
            top: 4,
            marginRight: 4
          },
          dateTouchBody: {
            flex: 1,
            height: 34
          },
          dateInput: {
            height: 34,
            marginRight: 36,
            marginLeft: 4,
            borderColor: Colors.inputBlueBorderColor,
            backgroundColor: "white",
            width: width - 6
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={date => {
          this.props.updateUnknownField(stateName,date);
        }}
      />
    );
  }
  renderTabOne() {
    let self = this;
    return (
      <View>
        <H3>Eos qui</H3>
        <GooglePlaceInput placeholder="Eos qui"
          onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
          self.props.updateUnknownField('gpInput1',data.description);
        }}
        getDefaultValue={() => {
          return self.props.unknown.gpInput1; // text input default value
        }} />
        <H3>Eos qui</H3>
        <GooglePlaceInput placeholder="Eos qui"
          onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
          self.props.updateUnknownField('gpInput2',data.description);
        }}
        getDefaultValue={() => {
          return self.props.unknown.gpInput2; // text input default value
        }} />
      </View>
    );
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.title}>What ever title</Text>
          <Button
            backgroundColor={Colors.primary}
            color="white"
            borderRadius={4}
            style={styles.topButton}
          >
            Eu Fugiate
          </Button>

        </View>
        {this.renderTabOne()}

        <H3>Ut enim</H3>
        <IconInput
          placeholder="Eu fugiat"
          inputStyle={styles.inputStyle}
          style={styles.IconInputStyle}
          iconContainerStyle={styles.iconContainerStyle}
          value={this.props.unknown.iconinput1}
          onChangeText={(v)=>this.props.updateUnknownField('iconinput1',v)}
        />
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 2 }}>
            <H3>Ut enim</H3>
            <ModalDropdown
              ref="dropdownContainer"
              dropdownTextStyle={{ fontSize: 14 }}
              adjustFrame={this.dropdownadjustFrame.bind()}
              onSelect={(i,v)=>this.props.updateUnknownField('dropdown1',v)}
              defaultValue={this.props.unknown.dropdown1 ||"Drop Down Menu" }
              style={{
                backgroundColor: "white",
                borderRadius: 4,
                borderColor: Colors.inputBlueBorderColor,
                height: 36,
                marginLeft: 1,
                borderWidth: 1,
                justifyContent: "center",
                paddingLeft: 4
              }}
              options={[
                "Drop Down Menu",
                "Drop Down Menu X",
                "Drop Down Menu Y"
              ]}
            />
          </View>
          <View style={{ flex: 1 }}>
            <H3>Ut enim</H3>
            <IconInput
              value={this.props.unknown.iconinput2}
              onChangeText={(v)=>this.props.updateUnknownField('iconinput2',v)}
              placeholder="Eu fugiat"
              inputStyle={styles.inputStyle}
              style={styles.IconInputStyle}
              iconContainerStyle={styles.iconContainerStyle}
            />
          </View>
          <View style={{ flex: 1 }}>
            <H3>Ut enim</H3>
            <IconInput
              value={this.props.unknown.iconinput3}
              onChangeText={(v)=>this.props.updateUnknownField('iconinput3',v)}
              placeholder="Eu fugiat"
              inputStyle={styles.inputStyle}
              style={styles.IconInputStyle}
              iconContainerStyle={styles.iconContainerStyle}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <H3>Ut enim</H3>
            <IconInput
              value={this.props.unknown.iconinput4}
              onChangeText={(v)=>this.props.updateUnknownField('iconinput4',v)}
              placeholder="Eu fugiat"
              inputStyle={styles.inputStyle}
              placeholder="1234"
              style={styles.IconInputStyle}
              iconContainerStyle={styles.iconContainerStyle}
            />
          </View>
          <View style={{ flex: 1 }}>
            <H3>Ut enim</H3>
            <IconInput
              value={this.props.unknown.iconinput5}
              onChangeText={(v)=>this.props.updateUnknownField('iconinput5',v)}
              placeholder="Eu fugiat"
              inputStyle={styles.inputStyle}
              placeholder="12"
              style={styles.IconInputStyle}
              iconContainerStyle={styles.iconContainerStyle}
            />
          </View>
        </View>

        <H3>Eu fugiata</H3>
        {this.renderDatePicker("d1", "YYYY-MM-DD HH:mm", "datetime")}
        <H3>Eu fugiata</H3>
        {this.renderDatePicker("d2", "YYYY-MM-DD", "date")}
        <H3>Eu fugiata</H3>
        {this.renderDatePicker("d3", "YYYY-MM-DD", "date")}
        <IconInput
          value={this.props.unknown.iconinput6}
          onChangeText={(v)=>this.props.updateUnknownField('iconinput6',v)}
          placeholder="Eu fugiat"
          inputStyle={[styles.inputStyle]}
          style={[styles.IconInputStyle, { marginTop: 8 }]}
          iconContainerStyle={styles.iconContainerStyle}
        />
        <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
          <Button
            backgroundColor={Colors.primary}
            color="white"
            borderRadius={4}
            style={styles.topButton}
          >
            Eu Fugiate
          </Button>
          <Button
            onPress={()=>this.props.mockRequestContainer(this.props.unknown)}
            backgroundColor={Colors.secondary}
            color="white"
            borderRadius={4}
            style={styles.topButton}
          >
            Eu Fugiate
          </Button>

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.grayBg,
    flex: 1
  },
  title: {
    flex: 1,
    fontSize: 17,
    padding: 10,
    fontWeight: "bold"
  },
  topButton: {
    marginBottom: 6,
    marginTop: 6,
    height: 34,
    paddingLeft: 25,
    paddingRight: 25
  },
  inputStyle: { paddingLeft: 10, height: 34, backgroundColor: "white" },
  IconInputStyle: { padding: 0, marginTop: 0, height: 37, marginBottom: 2 },
  iconContainerStyle: { maxWidth: 0, width: 0 }
});

const mapStateToProps = ({ group,unknown }) => {
  const { list } = group;
  return { list,unknown };
};

export default connect(mapStateToProps, {updateUnknownField,mockRequestContainer})(UnknownContainer);
