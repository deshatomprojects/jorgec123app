import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import PopupDialog, {SlideAnimation} from 'react-native-popup-dialog';
import {Colors} from '../themes';
import {requestToJoinGroup,toggleSideMenu} from '../actions';
import {Button} from '../components/common';
import RightButton from '../components/RightButton';

class Group extends Component {
    constructor(props) {
        super(props);
        this.state = {
            groupId: ''
        };
    }

    joinGroup(groupId) {
        this.props.requestToJoinGroup(groupId);
    }

    updateGroupId(v) {
        console.log(v);
    }

    render() {
        return (
            <View style={{
                flexDirection: 'row'
            }}>

                <RightButton onPress={() => this.props.toggleSideMenu()} iconName="ios-menu"/>
                <RightButton onPress={() => this.popupDialog.show()} iconName="ios-people"/>
                <PopupDialog overlayOpacity={0} ref={(popupDialog) => {
                    this.popupDialog = popupDialog;
                }} height={220} dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
                    <View style={styles.popupContainer}>
                        <Text style={styles.popupTitle}>Join A Group</Text>
                        <Text style={styles.description}>Please enter a group number provided by your Admin</Text>
                        <View style={styles.padding10}>
                            <View style={styles.textInputContainer}>
                                <TextInput underlineColorAndroid='rgba(0,0,0,0)' onChangeText={(v) => this.updateGroupId(v)} placeholder={'placeholder'} autoCorrect={false} style={styles.textInput}/>

                            </View>
                            <Button color="white" borderRadius={4} onPress={() => this.joinGroup(this.state.groupId)} disabled= {this.state.groupId === ''? true: false} backgroundColor={Colors.primary}>Ok</Button>

                        </View>

                    </View>
                </PopupDialog>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    popupTitle: {
        color: 'black',
        fontSize: 18,
        textAlign: 'center',
        padding: 10,
        backgroundColor: Colors.grayBg
    },
    padding10: {
        padding: 10
    },
    popupContainer: {
        borderWidth: 4,
        borderRadius: 8,
        flex: 1,
        marginRight: 20,
        borderColor: Colors.secondary
    },
    description: {
        color: Colors.grayText,
        fontSize: 18,
        textAlign: 'center',
        padding: 4
    },
    textInput: {
        flex: 1,
        borderColor: Colors.inputColor,
        fontSize: 14,
        height: 46,
        paddingLeft: 20
    },
    textInputContainer: {
        height: 46,
        flexDirection: 'row',
        margin: 4,
        borderColor: Colors.inputBorderColor,
        borderWidth: 1
    }
});

export default connect(null, {requestToJoinGroup, toggleSideMenu})(Group);
