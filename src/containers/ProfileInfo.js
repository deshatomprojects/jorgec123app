import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import _ from 'lodash';

import {connect} from 'react-redux';

import {updateProfileField, fetchUserProfile, updateProfile, updateMailField} from '../actions';
import {Button, H3, Toggle} from '../components/common';
import InlineInput from '../components/InlineInput';
import {AppStyles, Colors} from '../themes';

const BASIC_PROFILE_FIELDS = {
    first: {
        prop: 'first_name',
        required: true
    },
    middle: {
        prop: 'middle_name',
        required: false
    },
    last: {
        prop: 'last_name',
        required: true
    },
    street: {
        prop: 'street',
        required: false
    },
    city: {
        prop: 'city',
        required: false
    },
    state: {
        prop: 'state',
        required: false
    },
    zip: {
        prop: 'zip',
        required: false
    }
};

const MAILING_FIELDS = {
    first: {
        prop: 'first_name',
        required: true
    },
    middle: {
        prop: 'middle_name',
        required: false
    },
    last: {
        prop: 'last_name',
        required: true
    },
    street: {
        prop: 'street',
        required: false
    },
    city: {
        prop: 'city',
        required: false
    },
    zip: {
        prop: 'zip',
        required: false
    }
};

class ProfileInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            agreed: false
        }
    }

    componentWillMount() {
        this.props.fetchUserProfile();
    }

    toggleMailing() {
        if (this.props.profile.mailing === 'same') {
            this.props.updateProfileField({prop: 'mailing', value: {}});
        } else {
            this.props.updateProfileField({prop: 'mailing', value: 'same'});
        }
    }

    renderMailing() {
        if (this.props.profile.mailing === 'same') {
            return null;
        } else {
            return (
                <View>{_.map(BASIC_PROFILE_FIELDS, this.renderField.bind(this))}</View>
            )
        }
    }

    renderMailingFiled(fieldConfig, field) {
        const {prop, required} = fieldConfig;
        //console.log(this.props.profile.mailing[field])
        var mailing = {};
        return (<InlineInput key={field} title={_.upperFirst(field)} style={styles.inputStyle} value={this.props.profile.mailing[prop]} error={this.props.profile.mailing[prop + 'Err']} onChangeText={(v) => this.props.updateMailField({prop, value: v})} placeholder={_.upperFirst(field)} required={required}/>)
    }

    renderField(fieldConfig, field) {
        const {prop, required} = fieldConfig;
        return (<InlineInput key={field} title={_.upperFirst(field)} style={styles.inputStyle} value={this.props.profile[prop]} error={this.props.profile[prop + 'Err']} onChangeText={(v) => this.props.updateProfileField({prop, value: v})} placeholder={_.upperFirst(field)} required={required}/>)
    }

    render() {

        return (
            <ScrollView style={styles.container}>
                <View style={styles.inputContainer}>

                    <View style={styles.inputContainer}>
                        <H3>Basic Profile Info</H3>
                        {_.map(BASIC_PROFILE_FIELDS, this.renderField.bind(this))}

                    </View>
                    <View style={styles.agreeContainer}>
                        <Toggle style={{
                            width: 30
                        }} status={this.props.profile.mailing === 'same'
                            ? true
                            : false} onPressToggle={() => this.toggleMailing()}/>
                        <Text>
                            Mailing details are same as above
                        </Text>
                    </View>
                    {this.props.profile.mailing === 'same'
                        ? null
                        : <View style={styles.inputContainer}>
                            <H3>Mailing</H3>
                            {_.map(MAILING_FIELDS, this.renderMailingFiled.bind(this))}</View>
}

                    <Button color="white" onPress={() => this.props.updateProfile()} borderRadius={4} style={[
                        styles.buttonStyle, {
                            marginBottom: 4,
                            marginTop: 4,
                            height: 35,
                            paddingLeft: 25,
                            paddingRight: 25
                        }
                    ]} textStyle={styles.buttonTextStyle} backgroundColor={Colors.primary}>Update Profile</Button>
                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        ...AppStyles.screen.container,
        padding: 6
    },
    inputContainer: {
        padding: 4,
        margin: 8,
        marginTop: 10,
        ...AppStyles.center
    },
    buttonStyle: {
        height: 40,
        marginTop: 10,
        marginBottom: 10
    },
    hrStyle: {
        marginTop: 15,
        marginBottom: 15,
        paddingRight: 20,
        paddingLeft: 20
    },
    inputStyle: {
        marginTop: 2,
        marginBottom: 2
    },
    buttonTextStyle: {
        fontSize: 13,
        fontWeight: 'bold'
    },
    agreeContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

const mapStateToProps = ({profile}) => {
    return {profile};
};

export default connect(mapStateToProps, {updateProfileField, updateProfile, updateMailField, fetchUserProfile})(ProfileInfo);
