import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import _ from 'lodash';

import {connect} from 'react-redux';

import {updateRegisterForm} from '../actions';
import {Button, H3, Toggle} from '../components/common';
import InlineInput from '../components/InlineInput';
import {AppStyles, Colors} from '../themes';
import Picker from 'react-native-picker';

class ProfilePayment extends Component {
  constructor(props) {

      super(props);
      this.state={
        agreed: false,
        cartTypes: ['Visa', 'Master', 'Credit'],
      }
  }

  componentDidreaMount(){
    Picker.init({
        pickerData: this.state.cartTypes,
        selectedValue: [1],
        onPickerConfirm: data => {
            console.log(data);
        },
        onPickerCancel: data => {
            console.log(data);
        },
        onPickerSelect: data => {
            console.log(data);
        }
    });
  }

  render() {
    return (
        <ScrollView style={styles.container}>
          <View style={styles.inputContainer}>
          <H3>My Payments...</H3>
            <InlineInput
                        title="Card"
                        style={styles.inputStyle}
                        value={this.props.reg.first}
                        error={this.props.reg.firstErr}
                        onChangeText={(v) => this.props.updateRegisterForm({ prop: 'first', value: v })}
                        placeholder="Card" required />
            <InlineInput
                        title="Expires"
                        error={this.props.reg.middleErr}
                        onChangeText={(v) => this.props.updateRegisterForm({ prop: 'middle', value: v })}
                        style={styles.inputStyle}
                        value={this.props.reg.middle}
                        placeholder="Expires" />
            <InlineInput
                        title="Security Code"
                        error={this.props.reg.lastErr}
                        onChangeText={(v) => this.props.updateRegisterForm({ prop: 'last', value: v })}
                        style={styles.inputStyle}
                        value={this.props.reg.last}
                        placeholder="Security Code" required/>

            <InlineInput
                        title="Type"
                        editable={false}
                        onPress={()=>Picker.toggle()}
                        error={this.props.reg.phoneErr}
                        onChangeText={(v) => this.props.updateRegisterForm({ prop: 'phone', value: _.toLower(v) })}
                        style={styles.inputStyle}
                        value={this.props.reg.phone}
                        placeholder="Type" required/>


        <Button color="white" borderRadius={4} style={[styles.buttonStyle, {marginBottom: 4, marginTop: 4, height: 35, paddingLeft: 25, paddingRight: 25, }]} textStyle={styles.buttonTextStyle} backgroundColor={Colors.primary}>Update Payment</Button>
      </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
      ...AppStyles.screen.container,
      padding: 6,
  },
    inputContainer:{
      padding: 4,
      margin: 8,
      marginTop: 10,
      ...AppStyles.center,
    },
    buttonStyle: {
        height: 40,
        marginTop: 10,
        marginBottom: 10
    },
    hrStyle: {
        marginTop: 15,
        marginBottom: 15,
        paddingRight: 20,
        paddingLeft: 20,
    },
    inputStyle: {
        marginTop: 2,
        marginBottom: 2
    },
    buttonTextStyle: {
        fontSize: 13,
        fontWeight: 'bold'
    },
});

const mapStateToProps = ({auth, reg}) => {
    const {loading} = auth;
    return {loading, reg};
};

export default connect(mapStateToProps, {updateRegisterForm})(ProfilePayment);
