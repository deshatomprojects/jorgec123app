import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ListView,
  ScrollView,
  Dimensions
} from "react-native";
import { Button, H3 } from "../components/common";
import IconInput from "../components/IconInput";
import GooglePlaceInput from "../components/GooglePlaceInput";
import _ from "lodash";
import { AppStyles, Colors } from "../themes";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import ScrollableTabView from "react-native-scrollable-tab-view";
var { height, width } = Dimensions.get("window");
import { updateUnknownTabField, mockRequestTabContainer } from "../actions";
import DatePicker from "react-native-datepicker";

class UnknownContainer extends Component {
  constructor(props) {
    super(props);
  }
  renderDatePicker(stateName, format, model) {
var self = this;
    return (
      <DatePicker
        style={{
          borderColor: Colors.inputBlueBorderColor,
          width: width - 6,
          height: 34,
          marginTop: 0,
          marginBottom: 4
        }}
        date={this.props.unknownTab[stateName]}
        mode={model}
        placeholder="select date"
        format={format}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        customStyles={{
          dateIcon: {
            position: "absolute",
            right: 0,
            top: 4,
            marginRight: 4
          },
          dateTouchBody: {
            flex: 1,
            height: 34
          },
          dateInput: {
            height: 34,
            marginRight: 36,
            marginLeft: 4,
            borderColor: Colors.inputBlueBorderColor,
            backgroundColor: "white",
            width: width - 6
          }
          // ... You can check the source to find the other keys.
        }}
        onDateChange={date => {
          self.props.updateUnknownTabField(stateName, date);
        }}
      />
    );
  }
  renderTabOne() {
    var self = this;
    return (
      <View>
        <H3>Eos qui</H3>
        <GooglePlaceInput placeholder="Eos qui"
          onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
          self.props.updateUnknownTabField('gpInput1',data.description);
        }}
        getDefaultValue={() => {
          return self.props.unknownTab.gpInput1; // text input default value
        }} />
        <H3>Eos qui</H3>
        <GooglePlaceInput placeholder="Eos qui"
          onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
          self.props.updateUnknownTabField('gpInput2',data.description);
        }}
        getDefaultValue={() => {
          return self.props.unknownTab.gpInput2; // text input default value
        }} />
      </View>
    );
  }
  renderTabTwo() {
    var self = this;

    return (
      <View>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <H3>Eos qui</H3>
            <GooglePlaceInput placeholder="Eos qui"
              onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
              self.props.updateUnknownTabField('gpInput3',data.description);
            }}
            getDefaultValue={() => {
              return self.props.unknownTab.gpInput3; // text input default value
            }} />
          </View>
          <View>
            <H3>Eos qui</H3>
            <IconInput
              inputStyle={{ paddingLeft: 0, textAlign: "center", height: 37 }}
              style={{ padding: 0, marginTop: 0, height: 37 }}
              iconContainerStyle={{ maxWidth: 0, width: 0 }}
              value={this.props.unknownTab.iconinput6}
              onChangeText={(v)=>this.props.updateUnknownTabField('iconinput6',v)}
            />
          </View>
        </View>
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <H3>Eos qui</H3>
            <GooglePlaceInput
              onChangeText={() => alert("onnn")}
              placeholder="Eos qui"
              onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
              self.props.updateUnknownTabField('gpInput4',data.description);
            }}
            getDefaultValue={() => {
              return self.props.unknownTab.gpInput4; // text input default value
            }} />
          </View>
          <View>
            <H3>Eos qui</H3>
            <IconInput
              inputStyle={{ paddingLeft: 0, textAlign: "center", height: 37 }}
              style={{ padding: 0, marginTop: 0, height: 37 }}
              iconContainerStyle={{ maxWidth: 0, width: 0 }}
              value={this.props.unknownTab.iconinput7}
              onChangeText={(v)=>this.props.updateUnknownTabField('iconinput7',v)}
            />
          </View>
        </View>

      </View>
    );
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View style={{ flexDirection: "row" }}>
          <Text style={styles.title}>What ever title</Text>
          <Button
            backgroundColor={Colors.primary}
            color="white"
            borderRadius={4}
            style={styles.topButton}
          >
            Eu Fugiate
          </Button>

        </View>
        <View
          style={{
            flex: 1,
            margin: 10,
            borderRadius: 4,
            borderWidth: 1,
            borderColor: Colors.inputBlueBorderColor,
            backgroundColor: "white"
          }}
        >
          <ScrollableTabView
            style={{
              paddingBottom: 0
            }}
            tabBarTextStyle={{
              paddingBottom: 0,
              marginBottom: 0
            }}
            tabBarBackgroundColor="white"
            tabBarUnderlineStyle={{
              backgroundColor: Colors.inputBlueBorderColor,
              height: 3
            }}
            tabBarActiveTextColor={Colors.secondary}
            tabBarInactiveTextColor={Colors.inputBlueBorderColor}
            initialPage={1}
          >

            <View tabLabel="tabOne">
              {this.renderTabOne()}
            </View>
            <View tabLabel="tabTwo">
              {this.renderTabTwo()}
            </View>

          </ScrollableTabView>
        </View>

        <H3>Ut enim</H3>
        <IconInput
          placeholder="Eu fugiat"
          inputStyle={styles.inputStyle}
          style={styles.IconInputStyle}
          iconContainerStyle={styles.iconContainerStyle}
          value={this.props.unknownTab.iconinput1}
          onChangeText={(v)=>this.props.updateUnknownTabField('iconinput1',v)}
        />
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <H3>Ut enim</H3>
            <IconInput
              placeholder="Eu fugiat"
              inputStyle={styles.inputStyle}
              style={styles.IconInputStyle}
              iconContainerStyle={styles.iconContainerStyle}
              value={this.props.unknownTab.iconinput2}
              onChangeText={(v)=>this.props.updateUnknownTabField('iconinput2',v)}
            />
          </View>
          <View style={{ flex: 1 }}>
            <H3>Ut enim</H3>
            <IconInput
              placeholder="Eu fugiat"
              inputStyle={styles.inputStyle}
              style={styles.IconInputStyle}
              iconContainerStyle={styles.iconContainerStyle}
              value={this.props.unknownTab.iconinput3}
              onChangeText={(v)=>this.props.updateUnknownTabField('iconinput3',v)}
            />
          </View>
          <View style={{ flex: 1 }}>
            <H3>Ut enim</H3>
            <IconInput
              placeholder="Eu fugiat"
              inputStyle={styles.inputStyle}
              style={styles.IconInputStyle}
              iconContainerStyle={styles.iconContainerStyle}
              value={this.props.unknownTab.iconinput4}
              onChangeText={(v)=>this.props.updateUnknownTabField('iconinput4',v)}
            />
          </View>

        </View>
        <H3>Eu fugiata</H3>
        {this.renderDatePicker("d1", "YYYY-MM-DD", "date")}
        <H3>Eu fugiatax</H3>
        {this.renderDatePicker("d2", "YYYY-MM-DD", "date")}

        <IconInput
          placeholder="Eu fugiat"
          inputStyle={[styles.inputStyle]}
          style={[styles.IconInputStyle, { marginTop: 8 }]}
          iconContainerStyle={styles.iconContainerStyle}
          value={this.props.unknownTab.iconinput5}
          onChangeText={(v)=>this.props.updateUnknownTabField('iconinput5',v)}
        />
        <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
          <Button
            backgroundColor={Colors.primary}
            color="white"
            borderRadius={4}
            style={styles.topButton}
          >
            Eu Fugiate
          </Button>
          <Button
            backgroundColor={Colors.secondary}
            color="white"
            borderRadius={4}
            style={styles.topButton}
                    onPress={()=>this.props.mockRequestTabContainer(this.props.unknownTab)}
          >
            Eu Fugiate
          </Button>

        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.grayBg,
    flex: 1
  },
  title: {
    flex: 1,
    fontSize: 17,
    padding: 10,
    fontWeight: "bold"
  },
  topButton: {
    marginBottom: 6,
    marginTop: 6,
    height: 34,
    paddingLeft: 25,
    paddingRight: 25
  },
  inputStyle: { paddingLeft: 10, height: 34, backgroundColor: "white" },
  IconInputStyle: { padding: 0, marginTop: 0, height: 37, marginBottom: 2 },
  iconContainerStyle: { maxWidth: 0, width: 0 }
});

const mapStateToProps = ({ group, unknownTab }) => {
  const { list } = group;
  return { list, unknownTab };
};

export default connect(mapStateToProps, {
  updateUnknownTabField,
  mockRequestTabContainer
})(UnknownContainer);
