import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import _ from 'lodash';

import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {AppStyles, Colors} from '../themes';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import ProfileAdvanced from './ProfileAdvanced';
import ProfileInfo from './ProfileInfo';
import ProfilePayment from './ProfilePayment';

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            agreed: false
        }
    }

    render() {
        return (
            <ScrollableTabView style={{
                paddingBottom: 0
            }} tabBarTextStyle={{
                paddingBottom: 0,
                marginBottom: 0
            }} tabBarBackgroundColor={Colors.secondary} tabBarUnderlineStyle={{
                backgroundColor: 'white'
            }} tabBarActiveTextColor={'white'} tabBarInactiveTextColor={'white'} initialPage={1}>

                <ProfileAdvanced tabLabel='Advanced'/>
                <ProfileInfo tabLabel='Profile & Mail'/>
                <ProfilePayment tabLabel='Payment'/>

            </ScrollableTabView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        ...AppStyles.screen.container,
        padding: 6
    },
    inputContainer: {
        padding: 4,
        margin: 8,
        marginTop: 10,
        ...AppStyles.center
    },
    buttonStyle: {
        height: 40,
        marginTop: 10,
        marginBottom: 10
    },
    hrStyle: {
        marginTop: 15,
        marginBottom: 15,
        paddingRight: 20,
        paddingLeft: 20
    },
    inputStyle: {
        marginTop: 2,
        marginBottom: 2
    },
    buttonTextStyle: {
        fontSize: 13,
        fontWeight: 'bold'
    }
});

const mapStateToProps = ({auth}) => {
    const {logout} = auth;
    return {logout};
};

export default connect(mapStateToProps, {})(Profile);
