import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  ListView,
  DeviceEventEmitter,
  NativeAppEventEmitter,
  Platform
} from "react-native";
import { Button } from "../components/common";
import Card from "../components/Card";
import _ from "lodash";
import { AppStyles, Colors } from "../themes";
import { connect } from "react-redux";
import { fetchGroups, createNewGroupAndFetch } from "../actions";
import BackgroundTimer from "react-native-background-timer";
import { Actions } from "react-native-router-flux";
import uuid from "uuid";
var PushNotification = require("react-native-push-notification");
const EventEmitter = Platform.select({
  ios: () => NativeAppEventEmitter,
  android: () => DeviceEventEmitter
})();

class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.fetchGroups();
    this.createDataSource(this.props);
  }

  componentDidMount() {
    var self = this;
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: function(token) {
        this.props.fetchGroups();
      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: function(notification) {
        alert("onNotification");
      },

      // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
      senderID: "YOUR GCM SENDER ID",

      // IOS ONLY (optional): default: all - Permissions to register.
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
      * (optional) default: true
      * - Specified if permissions (ios) and token (android and ios) will requested or not,
      * - if not, you must call PushNotificationsHandler.requestPermissions() later
      */
      requestPermissions: true
    });

    BackgroundTimer.start(10000); // delay in milliseconds

    EventEmitter.addListener("backgroundTimer", () => {
      var newId = "new-" + uuid.v4();
      PushNotification.localNotificationSchedule({
         foreground: true,
        vibrate: true, // (optional) default: true
        message: "Jorgec123 - " + newId, // (required)
        date: new Date(Date.now() + 10 * 1000) // in 10 secs
      });
      self.forceUpdate();
      self.props.createNewGroupAndFetch(newId);
    });
  }

  componentWillReceiveProps(nextProps) {
    this.createDataSource(nextProps);
  }

  //TODO:remove this after demo
  demoPurposeOnly(list) {
    _.each(list, item => {
      if (_.random(0, 1)) {
        item.isInvitation = true;
      }
    });
    return _.orderBy(list, ["isInvitation"], ["ace"]);
  }

  createDataSource({ list }) {
    list = this.demoPurposeOnly(list); //TODO:remove this after demo
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });

    this.dataSource = ds.cloneWithRows(list);
  }

  render() {
    return (
      <View style={styles.container}>
        <ListView
          enableEmptySections
          style={styles.container}
          dataSource={this.dataSource}
          renderRow={data =>
            <Card
              title={data.id}
              isInvitation={data.isInvitation}
              content={data.name}
              onPress={() =>
                Actions.Groups({ data, title: `Group(${data.id})` })}
            />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.grayBg,
    flex: 1
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  }
});

const mapStateToProps = ({ group }) => {
  const { list } = group;
  return { list };
};

export default connect(mapStateToProps, {
  fetchGroups,
  createNewGroupAndFetch
})(Home);
