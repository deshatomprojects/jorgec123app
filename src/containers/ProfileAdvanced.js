import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import _ from 'lodash';

import {connect} from 'react-redux';
import {updateAuthField, updateEmail,updatePhone, updatePassword} from '../actions';
import {Button,H3} from '../components/common';
import InlineInput from '../components/InlineInput';
import {AppStyles, Colors} from '../themes';


class ProfileAdvanced extends Component {

    constructor(props) {
        super(props);
        this.state = {
            agreed: false
        }
    }

    render() {

        return (
            <ScrollView style={styles.container}>
                <View style={styles.inputContainer}>
                    <H3>UPDATE EMAIL</H3>
                    <InlineInput
                                title="Email"
                                style={styles.inputStyle}
                                value={this.props.email}
                                onChangeText={(v)  => this.props.updateAuthField({ prop: 'email', validate: 'email', value: _.toLower(v) , required: true })}
                                placeholder="Email"  />
                    <Button color="white"
                       disabled={this.props.emailErr? true: false}
                       onPress={()=>this.props.updateEmail(this.props.email)}
                       borderRadius={4} style={
                        styles.buttonStyle

                    } textStyle={styles.buttonTextStyle} backgroundColor={Colors.primary}>UPDATE EMAIL</Button>


                    <H3>UPDATE PHONE</H3>
                    <InlineInput
                      title="Phone"
                      style={styles.inputStyle}
                      value={this.props.phone}
                      required={true}

                      onChangeText={(v)  => this.props.updateAuthField({ prop: 'phone', value: v , required: true })}
                      placeholder="Phone"  />
                    <Button color="white"
                      onPress={()=>this.props.updatePhone(this.props.phone)}
                        disabled={this.props.phoneErr? true: false}
                        borderRadius={4} style={
                        styles.buttonStyle
                    } textStyle={styles.buttonTextStyle} backgroundColor={Colors.primary}>UPDATE PHONE</Button>

                    <H3>UPDATE PASSWORD</H3>
                    <InlineInput
                                title="Old"
                                style={styles.inputStyle}
                                secureTextEntry
                                value={this.props.old_password}
                                required={true}
                                onChangeText={(v)  => this.props.updateAuthField({ prop: 'old_password', value: v , required: true })}
                                placeholder="Old Password" required />
                    <InlineInput
                                title="New"
                                secureTextEntry
                                style={styles.inputStyle}
                                value={this.props.new_password}
                                required={true}
                                onChangeText={(v)  => this.props.updateAuthField({ prop: 'new_password', value: v , required: true })}
                                placeholder="New Password" required />
                    <Button color="white"
                        onPress={()=>this.props.updatePassword(this.props.old_password, this.props.new_password)}
                        disabled={(this.props.new_passwordErr || this.props.old_passwordErr )? true: false}
                       borderRadius={4} style={
                        styles.buttonStyle
                    } textStyle={styles.buttonTextStyle} backgroundColor={Colors.primary}>UPDATE PASSWORD</Button>



                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...AppStyles.screen.container,
        padding: 6
    },
    inputContainer: {
        padding: 4,
        margin: 8,
        marginTop: 10,
        ...AppStyles.center
    },
    buttonStyle: {
        height: 40,
        marginTop: 10,
        marginBottom: 10,
        marginBottom: 4,
        marginTop: 4,
        height: 35,
        paddingLeft: 25,
        paddingRight: 25
    },
    hrStyle: {
        marginTop: 15,
        marginBottom: 15,
        paddingRight: 20,
        paddingLeft: 20
    },
    inputStyle: {
        marginTop: 2,
        marginBottom: 2
    },
    buttonTextStyle: {
        fontSize: 13,
        fontWeight: 'bold',
    }
});

const mapStateToProps = ({auth, reg}) => {
    const {email,emailErr,  old_password, new_password, phone, new_passwordErr, phoneErr} = auth;
    return {email, emailErr, old_password, new_password, phone, new_passwordErr, phoneErr};
};

export default connect(mapStateToProps, {updateAuthField, updateEmail,updatePassword, updatePhone})(ProfileAdvanced);
