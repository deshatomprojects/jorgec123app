import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity,ScrollView, View, Text} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {AppStyles, Colors} from '../themes';
// import {} from '../actions'
import InlineInput from '../components/InlineInput'
class ManageReservation extends Component {
    constructor(props) {
        super(props);
    }
    renderUsers(){
      return _.map(this.props.reservations, (reservation, i)=>{
        return (
          <TouchableOpacity onPress={()=>Actions.EditReservation()} key={i} style={styles.reservationContainer}>
            <Text style={{fontWeight:'800' , fontSize:16}}>{reservation.dinner}</Text>
            <Text style={{color:'gray' , fontSize:10}}>{reservation.reservation}</Text>
          </TouchableOpacity>
        )
      });
    }
    render() {
        return (
            <ScrollView style={styles.container}>
            {this.renderUsers()}
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        ...AppStyles.screen.container,
        padding: 6
    },
    reservationContainer: {
        height: 60,
        margin: 8,
        marginBottom: 0,
        padding: 10,
        backgroundColor: 'white',
        borderColor: Colors.inputBorderColor,
        borderWidth: 0.5,
        borderRadius: 8
    },
});

const mapStateToProps = ({group}) => {
    const {reservations} = group;
    return {reservations};
};

export default connect(mapStateToProps, {})(ManageReservation);
