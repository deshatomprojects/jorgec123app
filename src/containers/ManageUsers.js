import React, {Component} from 'react';
import {StyleSheet, ScrollView, View, Text} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {AppStyles, Colors} from '../themes';
import {fetchUsersByGroupId, updateUserShare} from '../actions'
import InlineInput from '../components/InlineInput'
import {Button} from '../components/common'
class ManageUsers extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount(){
      this.props.fetchUsersByGroupId();
    }
    onChangeText(id, v=0){
      if(!v){v=0}
      var v = parseInt(v);
          if(  (0 <= v) && (v<=100)  ){
            this.props.updateUserShare(id, v);
          }
        this.forceUpdate();

    }
    renderUsers(){
      return _.map(this.props.users, (user, i)=>{
        return (
          <View key={i} style={styles.row}>
          <InlineInput
                keyboardType = 'numeric'
                title={`${user.name} - split %`}
                style={styles.inputStyle}
                value={`${user.share}`}
                onChangeText={(v) =>this.onChangeText(i, v)}
                titleContainerStyle={{maxWidth:300}}
                placeholder="0"  />
          </View>
        )
      });
    }
    render() {
        return (
            <ScrollView style={styles.container}>
            {this.renderUsers()}
            <Text style={styles.textError}>{this.props.userShareError}</Text>
            <Button
              color="white"
              borderRadius={4}
            disabled={this.props.userShareError? true: false}
              onPress={()=>alert('saved')}
              backgroundColor={Colors.primary}>SAVE</Button>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        ...AppStyles.screen.container,
        padding: 6
    },
    textError:{
      textAlign:'center',
      color:Colors.danger,
      fontSize: 11,
      padding:10,
      fontWeight: '100'
    }
});

const mapStateToProps = ({group}) => {
    const {users, userShareError} = group;
    return {users, userShareError};
};

export default connect(mapStateToProps, {fetchUsersByGroupId, updateUserShare})(ManageUsers);
