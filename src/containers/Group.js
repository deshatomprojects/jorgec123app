import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import _ from 'lodash';
import {Actions} from 'react-native-router-flux';
import {fetchUsersByGroupId} from '../actions'
import {connect} from 'react-redux';
import {Button, Hr} from '../components/common';
import Holder from '../components/Holder';
import GroupTotal from '../components/GroupTotal';
import InlineInput from '../components/InlineInput';
import {Colors} from '../themes';
import Icon from 'react-native-vector-icons/Ionicons';

class Group extends Component {
    constructor(props) {
        super(props);
        this.state = {
            agreed: false,
            cartTypes: ['Visa', 'Master', 'Credit']
        }
    }

    componentDidMount(){
      this.props.fetchUsersByGroupId();
    }

    acceptRequest(){
      let data = this.props.data;
      data.isInvitation = false;
      Actions.refresh({data})
    }

    renderShowNewGroup(){
      return (<ScrollView style={styles.container}>
              <View style={styles.inputContainer}>
                <Text style={styles.description}>{this.props.data.name}</Text>
                <View style={{flexDirection:'row', flex: 1}}>
                  <Button color="white" borderRadius={4} style={styles.acceptDeclineButton} textStyle={styles.buttonTextStyle} backgroundColor={Colors.primary} onPress={()=>this.acceptRequest()}>Accept</Button>
                  <Button color="white" borderRadius={4} style={styles.acceptDeclineButton} textStyle={styles.buttonTextStyle} backgroundColor={Colors.danger} onPress={()=>Actions.pop()}>Decline</Button>
                </View>
              </View>
              </ScrollView>)
    }

    renderUsers(){
      return _.map(this.props.users, (user, i)=>{
        return (
          <View key={i} style={styles.row}>
              <Text style={styles.field}>{user.name}</Text>
              <Text style={styles.value}>$ {user.price}</Text>
          </View>
        )
      });
    }

    render() {
      if(this.props.data.isInvitation){
        return this.renderShowNewGroup();
      }
        return (
            <ScrollView style={styles.container}>
                <View style={styles.inputContainer}>
                    <Text style={styles.description}>{this.props.data.name}</Text>
                    <Hr/>
                    <GroupTotal/>
                    <Hr/>
                    <Holder title={'Manage Tips'}>
                        <Button color="white" borderRadius={4} style={styles.acceptDeclineButton} textStyle={styles.buttonTextStyle} onPress={()=>Actions.ManageReservation()}  backgroundColor={Colors.gray}>Manage Tips</Button>
                    </Holder>
                    <Holder title={'Group Splits'}>

                        {this.renderUsers()}

                        <Button color="white" borderRadius={4} style={styles.acceptDeclineButton} textStyle={styles.buttonTextStyle} onPress={()=>Actions.ManageUsers()} backgroundColor={Colors.gray}>Manage Users</Button>

                    </Holder>
                    <Button color="white" borderRadius={4} style={styles.acceptDeclineButton}textStyle={styles.buttonTextStyle} backgroundColor={Colors.primary}>Charge Group</Button>
                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 6
    },
    inputContainer: {
        padding: 4,
        margin: 8,
        marginTop: 10
    },
    description: {
        fontSize: 13,

        textAlign: 'justify',
        fontWeight: '300',
        marginBottom: 8
    },

    buttonStyle: {
        height: 40,
        marginTop: 10,
        marginBottom: 10
    },
    hrStyle: {
        marginTop: 15,
        marginBottom: 15,
        paddingRight: 20,
        paddingLeft: 20
    },
    row: {
        flexDirection: 'row',
        flex: 1,
        marginTop: 2
    },
    field: {
        flex: 1,
        fontSize: 12,
        backgroundColor: Colors.grayBg,
        textAlign: 'center',
        paddingVertical: 8,
        fontWeight: '300'
    },
    value: {
        flex: 1,
        backgroundColor: 'white',
        fontSize: 14,
        color: Colors.secondary,
        textAlign: 'center',
        paddingVertical: 8
    },
    buttonTextStyle: {
        fontSize: 13,
        fontWeight: 'bold'
    },
    acceptDeclineButton:{
          marginBottom: 4,
          marginTop: 4,
          height: 35,
          paddingLeft: 25,
          paddingRight: 25,
          flex: 1
    }
});

const mapStateToProps = ({auth, group}) => {
    const {users} = group;
    return {users};
};

export default connect(mapStateToProps, {fetchUsersByGroupId})(Group);
