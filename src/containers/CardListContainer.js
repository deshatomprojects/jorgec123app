import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ListView,
  ScrollView,
  Dimensions
} from "react-native";
import _ from "lodash";
import { Button, H3, Hr } from "../components/common";
import NewCard from "../components/NewCard";
import { AppStyles, Colors } from "../themes";
import Icon from "react-native-vector-icons/Ionicons";
import {fetchMoreCards} from '../actions'
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";

class CardListContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    //this.props.fetchGroups();
    this.createDataSource(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this.createDataSource(nextProps);
  }
  onEndReached() {
    this.props.fetchMoreCards();
  }

  createDataSource({ newCardList }) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.dataSource = ds.cloneWithRows(newCardList);
  }
  render() {
    return (
      <View style={styles.container}>
        <ListView
          onEndReached={() => this.onEndReached()}
          enableEmptySections
          style={styles.container}
          dataSource={this.dataSource}
          renderRow={data =>
            <NewCard
              from={data.from}
              to={data.to}
              list={data.list}
              boxList={data.boxList}
            />}
        />
      </View>
    );
  }
}

const styles = {
  container: {
    backgroundColor: Colors.grayBg,
    flex: 1
  },
  cardHeader: {
    fontSize: 17,
    fontWeight: "600",
    color: Colors.secondary,
    margin: 2
  },
  seperatedList: {
    fontSize: 14,
    color: Colors.gray,
    marginLeft: 0,
    marginBottom: 4
  },
  box: {
    flex: 1,
    height: 50,
    borderColor: Colors.inputBorderColor,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 4,
    margin: 2,
    borderRadius: 8
  },
  boxContainer: {
    flex: 1,
    flexDirection: "row",
    borderRadius: 8
  },
  newContainerFooter: {
    flex: 1,
    flexDirection: "row",
    padding: 3,

    marginTop: 4,
    borderTopWidth: 1,
    borderTopColor: Colors.grayBg
  },
  newCardContainer: {
    margin: 8,
    marginBottom: 0,
    padding: 10,
    backgroundColor: "white",
    borderColor: Colors.inputBlueBorderColor,
    borderWidth: 0.5,
    borderRadius: 8
  },
  boxTitle: {
    fontSize: 16,
    fontWeight: "600",
    color: Colors.inputColor
  },
  boxTime: {
    fontSize: 12,
    color: Colors.grayText
  },
  footerTitle: {
    fontSize: 15,
    fontWeight: "600",
    color: Colors.grayText
  },
  footerTime: {
    fontSize: 12,
    color: Colors.grayText
  },
  btn: {
    height: 26,
    paddingLeft: 8,
    paddingRight: 8
  }
};

const mapStateToProps = ({ newCardList }) => {
  return { newCardList };
};

export default connect(mapStateToProps, {fetchMoreCards})(CardListContainer);
