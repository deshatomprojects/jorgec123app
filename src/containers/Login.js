import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {loginUser, updateLoginForm} from '../actions';
import {Button, Hr} from '../components/common';
import IconInput from '../components/IconInput';
import LoginContainer from '../components/LoginContainer';
import {AppStyles, Colors, Images} from '../themes';
import Icon from  'react-native-vector-icons/FontAwesome';
import {APP_NAME} from '../constant';
import KeyboardSpacer from 'react-native-keyboard-spacer';
class Login extends Component {
    constructor(props) {
        super(props);
    }
    componentDidUpdate() {
        if (this.props.access_token) {
            Actions.drawer();
        }
    }
    login() {
        this.props.loginUser(this.props.username, this.props.password);
    }

  render() {

    return (
      <LoginContainer>
          <View style={styles.inputContainer}>
              <IconInput onChangeText={(v) => this.props.updateLoginForm({ prop: 'username', value: _.toLower(v) })}
                           icon={<Icon name="check-circle"  size={18} color={this.props.usernameErr || !this.props.username? Colors.gray : Colors.primary} />}
                           style={styles.inputStyle} value={this.props.username}  placeholder="Username" />

               <IconInput onChangeText={(v) => this.props.updateLoginForm({ prop: 'password', value: v })}
                          icon={<Icon name="check-circle" size={18} color={this.props.passwordErr || !this.props.password? Colors.gray : Colors.primary} />}
                          style={styles.inputStyle} value={this.props.password} secureTextEntry placeholder="Password" />

               <Button isLoading={this.props.loading}  color="white"
                 borderRadius={4} style={styles.buttonStyle} textStyle={styles.buttonTextStyle}
                 backgroundColor={Colors.primary} disabled={!this.props.username || !this.props.password}
                 onPress={()=>this.login()}>Login</Button>
          </View>
          <View style={styles.bottomContainer}>
                <Hr/>
                <Text style={styles.grayText}>{'Don\'t Have an Account? '}</Text>
                <Button color="white" borderRadius={4} style={styles.buttonStyle} textStyle={styles.buttonTextStyle} backgroundColor={Colors.secondary}  onPress={()=>Actions.Register()}>Sign Up</Button>
          </View>
          <KeyboardSpacer/>
      </LoginContainer>
    );
  }
}

const styles = StyleSheet.create({
    buttonStyle: {
        height: 40,
        marginTop: 10,
        marginBottom: 10
    },
    inputStyle: {
        marginTop: 6,
        marginBottom: 6
    },
    buttonTextStyle: {
        fontSize: 13,
        fontWeight: 'bold'
    },
    forgotTextStyle: {
        fontSize: 12,
        textDecorationLine: 'underline'
    },
    inputContainer: {flex: 1},
    bottomContainer: {
        flex: 1,
        justifyContent: 'flex-end'
    },
    grayText: {
        textAlign: 'center',
        fontSize: 12,
        color: Colors.grayText
    }
});

const mapStateToProps = ({login, auth}) => {
    const {loading, username, usernameErr, password, passwordErr} = login;
    const {access_token} = auth;
    return {
        loading, username, usernameErr, password, passwordErr, access_token
    };
};

export default connect(mapStateToProps, {loginUser, updateLoginForm})(Login);
