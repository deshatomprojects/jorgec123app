import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {updateRegisterForm, hasTypeError, registerUser} from '../actions';
import {Button, Hr, Toggle} from '../components/common';
import IconInput from '../components/IconInput';
import InlineInput from '../components/InlineInput';
import RegisterContainer from '../components/RegisterContainer';
import {AppStyles, Colors, Images} from '../themes';
import Icon from 'react-native-vector-icons/Ionicons';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {APP_NAME} from '../constant';

class Register extends Component {
  constructor(props) {
      super(props);
      this.state={
        agreed: false
      }
  }

  render() {
    return (
      <RegisterContainer>
        <View style={styles.inputContainer}>

          <InlineInput
                      title="Username"
                      style={styles.inputStyle}
                      value={this.props.reg.username}
                      error={this.props.reg.usernameErr}
                      onChangeText={(v) => this.props.updateRegisterForm({ prop: 'username', value: _.toLower(v) })}
                      placeholder="Username" required />
          <InlineInput
                      title="First"
                      style={styles.inputStyle}
                      value={this.props.reg.first}
                      error={this.props.reg.firstErr}
                      onChangeText={(v) => this.props.updateRegisterForm({ prop: 'first', value: v })}
                      placeholder="First" required />
          <InlineInput
                      title="Middle"
                      error={this.props.reg.middleErr}
                      onChangeText={(v) => this.props.updateRegisterForm({ prop: 'middle', value: v })}
                      style={styles.inputStyle}
                      value={this.props.reg.middle}
                      placeholder="Middle" />
          <InlineInput
                      title="Last"
                      error={this.props.reg.lastErr}
                      onChangeText={(v) => this.props.updateRegisterForm({ prop: 'last', value: v })}
                      style={styles.inputStyle}
                      value={this.props.reg.last}
                      placeholder="Last" required/>

          <InlineInput
                      title="Email"
                      keyboardType="email-address"
                      error={this.props.reg.emailErr}
                      onChangeText={(v) => this.props.updateRegisterForm({ prop: 'email', value: _.toLower(v), validate:'email'})}
                      style={styles.inputStyle}
                      value={this.props.reg.email}
                      placeholder="email@exmaple.com" required/>
          <InlineInput
                      title="Phone"
                      keyboardType="phone-pad"
                      error={this.props.reg.phoneErr}
                      onChangeText={(v) => this.props.updateRegisterForm({ prop: 'phone', value: _.toLower(v) })}
                      style={styles.inputStyle}
                      value={this.props.reg.phone}
                      placeholder="Phone" required/>

          <Hr style={styles.hrStyle}/>

          <InlineInput
                      title="Password"
                      secureTextEntry
                      error={this.props.reg.passwordErr}
                      onChangeText={(v) => this.props.updateRegisterForm({ prop: 'password', value: v })}
                      style={styles.inputStyle}
                      value={this.props.reg.password}
                      placeholder="Password" required/>
          <InlineInput
                      title="Verify"
                      secureTextEntry
                      error={this.props.reg.passwordVerifyErr}
                      onChangeText={(v) => this.props.updateRegisterForm({ prop: 'passwordVerify', value: v })}
                      style={styles.inputStyle}
                      value={this.props.reg.passwordVerify}
                      placeholder="PasswordVerify" required/>
                      <Hr style={styles.hrStyle}/>

          <View style={styles.agreeContainer}>
            <Toggle style={{width: 30}} status={this.state.agreed} onPressToggle={()=> this.setState({agreed: !this.state.agreed})} />
            <Text> Agree to Terms </Text>
          </View>

          <Button isLoading={this.props.reg.loading} color="white" borderRadius={4} style={styles.buttonStyle} textStyle={styles.buttonTextStyle} backgroundColor={Colors.primary} disabled={this.props.hasTypeError()|| !this.state.agreed} onPress={()=>this.props.registerUser()}>Sign Up Now</Button>


          <Button color={Colors.grayText} style={styles.hasAccountStyle} textStyle={styles.hasAccountTextStyle} backgroundColor={'white'} onPress={()=>Actions.pop()}>{' Already have an account? '}</Button>



        </View>
        <KeyboardSpacer/>
      </RegisterContainer>
    );
  }
}
const styles = StyleSheet.create({

    agreeContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonStyle: {
        height: 40,
        marginTop: 10,
        marginBottom: 10
    },
    hrStyle: {
        marginTop: 15,
        marginBottom: 15
    },
    inputStyle: {
        marginTop: 2,
        marginBottom: 2
    },
    buttonTextStyle: {
        fontSize: 13,
        fontWeight: 'bold'
    },
    hasAccountStyle: {
        marginBottom: 15,
    },
    hasAccountTextStyle:{
      fontSize:12,
      fontWeight: '300'
    }
});

const mapStateToProps = ({auth, reg}) => {
    return {reg};
};

export default connect(mapStateToProps, {updateRegisterForm, registerUser, hasTypeError})(Register);
