import React, {Component} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';
import {createGroup} from '../actions'
import {Button} from '../components/common';
import TextArea from '../components/TextArea';
import {Colors} from '../themes';
import Icon from 'react-native-vector-icons/Ionicons';

class Group extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        };
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.inputContainer}>
                    <TextArea icon={< Icon name = "ios-checkmark" size = {
                        28
                    }
                    color = {
                        Colors.primary
                    } />} style={styles.inputStyle} value={this.state.name} onChangeText={(v) => this.setState({name: v})} placeholder="Enter Your Group description"/>
                    <Button color="white" borderRadius={4} disabled={this.state.name === ''
                        ? true
                        : false} style={[styles.buttonStyle]} textStyle={styles.buttonTextStyle} onPress={() => this.props.createGroup(this.state.name)} backgroundColor={Colors.primary}>Save</Button>
                </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        padding: 6
    },
    inputContainer: {
        padding: 4,
        margin: 8,
        marginTop: 10,
        flex: 1
    },
    buttonStyle: {
        height: 40,
        marginTop: 10,
        marginBottom: 10
    },
    buttonTextStyle: {
        fontSize: 13,
        fontWeight: 'bold'
    }
});

export default connect(null, {createGroup})(Group);
