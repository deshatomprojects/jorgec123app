import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  ListView,
  ScrollView,
  Dimensions
} from "react-native";
import _ from "lodash";
import { Button, H3, Hr } from "../components/common";
import IconInput from "../components/IconInput";
import { AppStyles, Colors } from "../themes";
import { Table, Row } from "../components/LightTable";
import { connect } from "react-redux";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/Ionicons";
import MapView from 'react-native-maps';
const { width, height } = Dimensions.get('window');
import NewCardX from "../components/NewCard";
const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const SPACE = 0.01;


const DetailTopContainer = ({detailTopTitle, detailTopSubTitle, buttons})=>{
  return(
    <View style={styles.DetailTopContainer}>
      <View style={{ flex: 1 }} >
        <Text style={styles.detailTopTitle}>{detailTopTitle}</Text>
        <Text style={styles.detailTopSubTitle}>{detailTopSubTitle}</Text>
      </View>
      <View style={styles.smallBtnContainer} >
        {buttons.map((item, i) => {
          return (
            <Button
              key={i}
              backgroundColor={Colors.secondary}
              color="white"
              borderRadius={4}
              style={styles.smallBtn}> {item}</Button>
          )
        })}
      </View>
    </View>
  )
}

const BoxList = ({boxList})=>{
  return(
    <View style={styles.boxContainer}>
      {boxList.map((item, i) => {
        return (
          <View key={i} style={styles.box}>
            <Text style={styles.boxTitle}>
              {item.icon
                ? <Icon name={item.icon} size={12} color={Colors.secondary} />
                : null}
              {` ${item.title} `}
            </Text>
            <Text style={styles.boxTime}>
              {item.time}
            </Text>
          </View>
        );
      })}
    </View>
  )
}

const NewCard = ({ from, to, list, boxList}) => {
  return (
    <View style={{      borderColor: Colors.grayBg,
          borderTopWidth: 0.5}}>
      <Text style={styles.cardHeader}>
        {from}{" "}
        <Icon
          padding={10}
          name="ios-arrow-forward"
          size={18}
          color={Colors.secondary}
        />{" "}
         {to}
      </Text>
      <Text style={styles.seperatedList}>
        {" "}{list.join(" / ")}
      </Text>
    </View>
  );
};

class CardListContainer extends Component {

    constructor(props) {
      super(props);

      this.state = {
        a: {
          latitude: LATITUDE + SPACE,
          longitude: LONGITUDE + SPACE,
        },
        b: {
          latitude: LATITUDE - SPACE,
          longitude: LONGITUDE - SPACE,
        },
        data:  {
            from: 'NY',
            to: 'Amsterdam',
            list: ["NY", "London", "Amsterdam"],
            boxList: [
              { title: "NY", time: "7.30PM" },
              { title: "RTX", time: "2.30PM" },
              { title: "ZhQ", time: "3.30PM" },
              { title: "RKL", time: "1.40PM", icon: "md-time" }
            ]
          }
      };
    }

  render() {
    return (
    <ScrollView style={styles.container} >
      <View style={styles.MainCardContainer}>
        <DetailTopContainer detailTopTitle="Whatever Top Title" detailTopSubTitle="size" buttons={['1x','2x','3x','4x']}/>
        <NewCard
            from= 'NY, USA'
            to= 'Amsterdam,ND '
            list= {["NY", "London", "Amsterdam"]}

          />
          <BoxList boxList= {[
          { title: "NY", time: "7.30PM" },
          { title: "RTX", time: "2.30PM" },
          { title: "RKL", time: "1.40PM", icon: "md-time" },
        ]}/>
          <BoxList boxList= {[
            { title: "ChZ", time: "7.30PM" },
            { title: "TEI", time: "1.30PM", icon: "md-time" },
            { title: "CRZ", time: "8.30PM" },
        ]}/>

        <Table>
          <Row leftTitle="Confirmed" rightContent={<Button
                                                    backgroundColor={Colors.secondary}
                                                    color="white"
                                                    borderRadius={4}
                                                    style={styles.smallBtn}> Yes</Button>}/>
          <Row leftTitle="Onboarding" rightTitle="Yes" />
          <Row leftTitle="Family Members" rightTitle="Brother, Sister, Father in Law, Grand pa, grand ma" />
          <Row leftTitle="Reason to Fly" rightTitle="Because I am a bird." />
        </Table>

        <View style={{ flex: 1, padding: 10 }} >
          <Text style={styles.detailTopTitle}>What is My Title</Text>
          <Button
            backgroundColor={Colors.secondary}
            color="white"
            borderRadius={4}
            style={[styles.btn,styles.letsDo]}> Let's Do it</Button>
        </View>
        <View style={{ flex: 1, paddingLeft: 10 }} >
          <Text style={[styles.detailTopTitle]}>Get or Die</Text>
          <Text style={[styles.detailTopTitle, {fontSize:14}]}>What ever it is</Text>
          <Text style={[styles.detailTopTitle, {fontSize:14}]}>This should be table title</Text>
        </View>
        <Table>
          <Row leftTitle="Onboarding" rightTitle="Yes" />
          <Row leftTitle="Reason to Fly" rightTitle="Because I am a bird." />
        </Table>

        <View style={{  flexDirection:'row' , paddingTop: 15, paddingBottom: 10}}>
          <IconInput style={{flex: 1,height: 34, padding:0, margin:1}} title="Username" required />
          <Button
                  backgroundColor={Colors.secondary}
                  color="white"
                  borderRadius={4}
                  style={[styles.btn, {padding:0, height: 34}]}>Approved</Button>
        </View>

        <Button style={{borderWidth:1, borderColor:Colors.inputBorderColor, width: 120, height: 28}} borderRadius={4}> White Btn</Button>
        <View style={{ flex: 1, paddingLeft: 10 }} >
          <Text style={[styles.detailTopTitle]}>Get or Die</Text>
          <Text style={[styles.detailTopTitle, {fontSize:14}]}>What ever it is</Text>
        </View>
        <View style={{  flexDirection:'row' , paddingTop: 10}}>
          <IconInput style={{flex: 1,height: 34, padding:0, margin:1}} title="Username" required />
          <Button
                  backgroundColor={Colors.secondary}
                  color="white"
                  borderRadius={4}
                  style={[styles.btn, {padding:0, height: 34}]}>Not Approved</Button>
        </View>


        <View style={{ flex: 1, padding: 10 }} >
          <Text style={[styles.detailTopTitle]}>Get or Die</Text>
          <Text style={[styles.detailTopTitle, {fontSize:14}]}>What ever it is</Text>
        </View>

        <Table>
          <Row leftTitle="Onboarding" rightTitle="Yes" />
          <Row leftTitle="Onboarding" rightTitle="Yes" />
          <Row leftTitle="Family Members" rightTitle="Brother, Sister, Father in Law, Grand pa, grand ma" />
          <Row leftTitle="Reason to Fly" rightTitle="Because I am a bird." />
        </Table>

        <View style={{ flex: 1, padding: 10 }} >
          <Text style={[styles.detailTopTitle]}>Map</Text>
          <MapView
            initialRegion={{
              latitude: LATITUDE,
              longitude: LONGITUDE,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            }}
          style={{height: 300, flex:1}}
        >
          <MapView.Marker
             coordinate={this.state.a}
             draggable
           />
        </MapView>

        </View>


        <View style={{ flex: 1, paddingLeft: 10 }} >
          <Text style={[styles.detailTopTitle]}>Last Title</Text>
        </View>
        <NewCardX
          from={this.state.data.from}
          to={this.state.data.to}
          list={this.state.data.list}
          boxList={this.state.data.boxList}
        />
      </View>
    </ScrollView>
  );
  }
}

const styles = {
  container: {
    backgroundColor: Colors.grayBg,
    flex: 1
  },
  cardHeader: {
    fontSize: 17,
    fontWeight: "600",
    color: Colors.secondary,
    textAlign:'center',
    margin: 2,
    marginTop: 6,

  },
  seperatedList: {
    fontSize: 14,
    color: Colors.gray,
    marginLeft: 0,
    marginBottom: 4,
    textAlign:'center'
  },
  box: {
    flex: 1,
    height: 50,
    borderColor: Colors.inputBorderColor,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 4,
    margin: 2,
    borderRadius: 8
  },
  boxContainer: {
    flex: 1,
    flexDirection: "row",
    borderRadius: 8
  },
  DetailTopContainer: {
    flex: 1,
    flexDirection: "row",
    padding: 3,
    marginTop: 6,
  },
  MainCardContainer: {
    margin: 8,
    marginBottom: 8,
    padding: 4,
    backgroundColor: "white",
    borderColor: Colors.inputBlueBorderColor,
    borderWidth: 0.5,
    borderRadius: 8
  },
  CardContainer: {
    margin: 8,
    marginBottom: 0,
    padding: 4,
    backgroundColor: "white",
    borderColor: Colors.inputBlueBorderColor,
    borderWidth: 0.5,
    borderRadius: 8
  },
  boxTitle: {
    fontSize: 16,
    fontWeight: "600",
    color: Colors.inputColor
  },
  boxTime: {
    fontSize: 12,
    color: Colors.grayText
  },
  detailTopTitle: {
    fontSize: 18,
    fontWeight: "600",
    color: Colors.grayText
  },
  detailTopSubTitle: {
    fontSize: 12,
    color: Colors.grayText,
    textAlign: 'right'
  },
  btn: {
    height: 26,
    paddingLeft: 8,
    paddingRight: 8
  },
  smallBtn: {
    height: 26,
    paddingLeft: 4,
    paddingRight: 6,
    marginRight:0
  },
  smallBtnContainer:{
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingTop: 6
  },
  letsDo:{marginLeft:0, marginTop: 4, width: 130}
};

const mapStateToProps = ({ newCardList }) => {
  return { newCardList };
};

export default connect(mapStateToProps, {})(CardListContainer);
