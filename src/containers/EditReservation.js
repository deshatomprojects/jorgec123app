import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity,ScrollView, View, Text} from 'react-native';
import _ from 'lodash';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import {AppStyles, Colors} from '../themes';

import {updateReservation} from '../actions'
import KeyboardSpacer from 'react-native-keyboard-spacer';

import InlineInput from '../components/InlineInput'
import {Button} from '../components/common'

const RESERVATION_FIELDS = {
    title: {
        prop: 'title',
        required: true
    },
    provider: {
        prop: 'provider',
        required: true
    },
    Account: {
        prop: 'Account',
        required: true
    },
    AccountHolder: {
        prop: 'AccountHolder',
        required: true
    },
    amount:{
      prop: 'Amount',
      required: true,
      type: 'phone-pad'
    },
    Address: {
        prop: 'Address',
        required: true
    },
    city: {
        prop: 'city',
        required: true
    },
    state: {
        prop: 'state',
        required: true
    },
    zip: {
        prop: 'zip',
        required: true
    }
};
class EditReservation extends Component {
    constructor(props) {
        super(props);
    }

    renderReservationField(fieldConfig, field) {
        const {prop, required, type} = fieldConfig;
        if(!this.props.reservations) return;
        return (<InlineInput
          titleContainerStyle={{maxWidth:300}}
                  key={field}
                  title={_.upperFirst(field)}
                  style={styles.inputStyle}
                  keyboardType={type}
                  value={prop? this.props.reservations[prop] : ''}
                  onChangeText={(v) => this.props.updateReservation({prop, value: v})}
                  placeholder={_.upperFirst(field)} required={required}/>)
    }
    render() {
        return (
            <ScrollView style={styles.container}>
              {_.map(RESERVATION_FIELDS, this.renderReservationField.bind(this))}

              <Button
                color="white"
                borderRadius={4}
                onPress={()=>alert('saved')}
                backgroundColor={Colors.primary}>save</Button>
                <KeyboardSpacer/>

            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        ...AppStyles.screen.container,
        padding: 6
    },
    reservationContainer: {
        height: 60,
        margin: 8,
        marginBottom: 0,
        padding: 10,
        backgroundColor: 'white',
        borderColor: Colors.inputBorderColor,
        borderWidth: 0.5,
        borderRadius: 8
    },
});

const mapStateToProps = ({group}) => {
    const {reservations} = group;
    return {reservations};
};

export default connect(mapStateToProps, {updateReservation})(EditReservation);
