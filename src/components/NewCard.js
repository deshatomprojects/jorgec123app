import React from 'react';
import {View, Text} from 'react-native';
import { Button, H3, Hr } from "../components/common";
import { AppStyles, Colors } from "../themes";
import Icon from "react-native-vector-icons/Ionicons";


const NewCard = ({ from, to, list, boxList }) => {


    return (
      <View style={styles.newCardContainer}>
        <Text style={styles.cardHeader}>
          {from}{" "}
          <Icon
            padding={10}
            name="ios-arrow-forward"
            size={18}
            color={Colors.secondary}
          />{" "}
          {to}
        </Text>
        <Text style={styles.seperatedList}>
          {" "}{list.join(" / ")}
        </Text>

        <View style={styles.boxContainer}>
          {boxList.map((item, i) => {
            return (
              <View key={i} style={styles.box}>
                <Text style={styles.boxTitle}>
                  {item.icon
                    ? <Icon name={item.icon} size={12} color={Colors.secondary} />
                    : null}
                  {` ${item.title} `}
                </Text>
                <Text style={styles.boxTime}>
                  {item.time}
                </Text>
              </View>
            );
          })}
        </View>
        <View style={styles.newContainerFooter}>
          <View style={{ flex: 1 }}>
            <Text style={styles.footerTitle}>Whatever Title</Text>
            <Text style={styles.footerTime}>Whatever subtitle</Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-end",
              paddingTop: 6
            }}
          >
            <Button
              backgroundColor={Colors.primary}
              color="white"
              borderRadius={4}
              style={styles.btn}
            >
              OK
            </Button>
            <Button
              backgroundColor={Colors.secondary}
              color="white"
              borderRadius={4}
              style={styles.btn}
            >
              Cancel
            </Button>
          </View>
        </View>
      </View>
    );
};
const styles = {
  container: {
    backgroundColor: Colors.grayBg,
    flex: 1
  },
  cardHeader: {
    fontSize: 17,
    fontWeight: "600",
    color: Colors.secondary,
    margin: 2
  },
  seperatedList: {
    fontSize: 14,
    color: Colors.gray,
    marginLeft: 0,
    marginBottom: 4
  },
  box: {
    flex: 1,
    height: 50,
    borderColor: Colors.inputBorderColor,
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 4,
    margin: 2,
    borderRadius: 8
  },
  boxContainer: {
    flex: 1,
    flexDirection: "row",
    borderRadius: 8
  },
  newContainerFooter: {
    flex: 1,
    flexDirection: "row",
    padding: 3,

    marginTop: 4,
    borderTopWidth: 1,
    borderTopColor: Colors.grayBg
  },
  newCardContainer: {
    margin: 8,
    marginBottom: 0,
    padding: 10,
    backgroundColor: "white",
    borderColor: Colors.inputBlueBorderColor,
    borderWidth: 0.5,
    borderRadius: 8
  },
  boxTitle: {
    fontSize: 16,
    fontWeight: "600",
    color: Colors.inputColor
  },
  boxTime: {
    fontSize: 12,
    color: Colors.grayText
  },
  footerTitle: {
    fontSize: 15,
    fontWeight: "600",
    color: Colors.grayText
  },
  footerTime: {
    fontSize: 12,
    color: Colors.grayText
  },
  btn: {
    height: 26,
    paddingLeft: 8,
    paddingRight: 8
  }
};

export default NewCard;
