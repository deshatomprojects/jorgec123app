import React from 'react';
import {TextInput, View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import FontAwesome from  'react-native-vector-icons/FontAwesome';
import {Colors} from '../themes';
const INPUT_HEIGHT = 46;
const InlineInput = ({
    label,
    value,
    onChangeText,
    placeholder,
    secureTextEntry,
    titleContainerStyle,
    title,
    required,
    style,
    error,
    keyboardType,
    props,
    editable,
    onPress
}) => {
    const {
        inputStyle,
        labelStyle,
        containerStyle,
        iconContainerStyle,
        iconStyle,
        titleContainer,
        titleStyle,
        requiredIconStyle
    } = styles;

    return (
        <TouchableOpacity style={[containerStyle, style]} onPress={onPress}>
            <View style={[titleContainer, titleContainerStyle]}>
                {required
                    ? (
                        <View style={requiredIconStyle}>
                            <Icon name="ios-star-outline" size={10} color={'red'}/>
                        </View>
                    )
                    : null
                }
                <Text style={titleStyle} numberOfLines={1}>
                    {title}
                </Text>
            </View>

            <TextInput {...props} underlineColorAndroid='rgba(0,0,0,0)' secureTextEntry={secureTextEntry} placeholder={placeholder} editable={editable} autoCorrect={false} style={[
                inputStyle, {
                    color: secureTextEntry
                        ? Colors.primary
                        : Colors.inputColor
                }
            ]} value={value} keyboardType={keyboardType} onChangeText={onChangeText}/>
            <View style={iconContainerStyle}>
                <FontAwesome name="check-circle" size={18} color={(error || !value)
                    ? Colors.gray
                    : Colors.primary}/>
            </View>

        </TouchableOpacity>
    );
};
const styles = {
    iconContainerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: 34
    },
    requiredIconStyle: {
        position: 'absolute',
        left: 4,
        top: 4
    },
    titleContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        maxWidth: 70,
        paddingLeft: 6,
        borderRightWidth: 1,
        borderColor: Colors.inputBorderColor,
        height: INPUT_HEIGHT
    },
    titleStyle: {
        fontSize: 11,
        color: Colors.inputColor
    },
    iconStyle: {
        maxWidth: INPUT_HEIGHT,
        maxHeight: INPUT_HEIGHT
    },
    inputStyle: {
        flex: 1,
        borderColor: Colors.inputColor,
        fontSize: 14,
        height: INPUT_HEIGHT,
        paddingLeft: 10
    },
    containerStyle: {
        height: INPUT_HEIGHT,
        flexDirection: 'row',
        margin: 4,
        borderColor: Colors.inputBlueBorderColor,
       borderWidth: 1,
       borderRadius: 4,
    }
};

export default InlineInput;
