import React, {
  Component
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
import {
  AppStyles,
  Images
} from '../themes';
import {
  APP_NAME
} from '../constant';
class LoginContainer extends Component {

  onLayout(e) {
    const { nativeEvent: { layout: { height } } } = e;
    this.height = height;
    this.forceUpdate();
  }

  render() {
    const {height: heightOfDeviceScreen} = Dimensions.get('window');
    return (
        <ScrollView contentContainerStyle={{minHeight: this.height || heightOfDeviceScreen, padding: 15}}
                    onLayout={this.onLayout.bind(this)}>
            <View style={styles.logoContainer}>
                <Image style={styles.imageStyle} source={Images.logo}></Image>
                <Text style={styles.titleStyle}>{APP_NAME}</Text>
                <Text style={{
                    ...AppStyles.h3,
                    ...AppStyles.lightText
                }}>{'Join Our Community'}</Text>
            </View>
            {this.props.children}
        </ScrollView>
    );
  }

};

const styles = StyleSheet.create({
    logoContainer: {
        ...AppStyles.center,
        flex: 2
    },
    titleStyle: {
        ...AppStyles.h1,
        margin: 10
    },
    imageStyle: {
        width: 90,
        height: 90
    }
});

export default LoginContainer;
