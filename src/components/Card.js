import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {Colors} from '../themes';
const Card = ({title, style,isInvitation, content, onPress}) => {
    const {containerStyle, titleStyle, invitationStyle,descriptionStyle} = styles;

    return (
        <TouchableOpacity activeOpacity={0.7} onPress={onPress} style={[containerStyle, style]}>
            <Text style={[titleStyle]}>{title}</Text>
            {isInvitation? <Text style={invitationStyle}>{'invitation'}</Text>:null}

            <Text numberOfLines={3} style={[descriptionStyle]}>{content}
            </Text>
        </TouchableOpacity>
    );
};
const styles = {
    containerStyle: {
        height: 90,
        margin: 8,
        marginBottom: 0,
        padding: 10,
        backgroundColor: 'white',
        borderColor: Colors.inputBorderColor,
        borderWidth: 0.5,
        borderRadius: 8
    },
    invitationStyle:{
      position:'absolute',
      top:4,
      right: 6,
      color:Colors.secondary,
      fontWeight:'bold'
    },
    titleStyle: {
        fontSize: 18,
        fontWeight: '600'
    },
    descriptionStyle: {
        fontSize: 13,
        padding: 4,
        paddingTop: 0,
        textAlign: 'justify',
        color: Colors.gray,
        fontWeight: '300'
    }

};

export default Card;
