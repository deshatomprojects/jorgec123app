import React from 'react';
import {View, Text} from 'react-native';
import {Colors} from '../themes';

const Row = ({leftContent, leftTitle, rightTitle, rightContent}) =>{
  return (
    <View style={styles.row}>
      <View style={[styles.flex, {justifyContent:'center'}]}><Text>{leftTitle}</Text>{leftContent}</View>
      <View style={styles.flex}><Text>{rightTitle}</Text>{rightContent}</View>
    </View>
  )
}

const Table = ({title, children, style}) => {
    return (
        <View style={[styles.containerStyle, style]}>
          {children}
        </View>
    );
};
const styles = {
    containerStyle: {
        borderColor: Colors.inputBorderColor,
        backgroundColor:'#fdfdfd',
        padding: 1,
        margin:4,
        marginTop:8
    },
    titleStyle: {
        fontSize: 14,
        backgroundColor: Colors.grayBg,
        textAlign: 'center',
        paddingVertical: 8,
        fontWeight: '400'
    },
    flex: {
      flex:1,
      padding: 4,
    },
    row: {
      flex:1,
      flexDirection:'row',
      borderColor: Colors.inputBorderColor,
      borderWidth:0.5,
      paddingTop: 6,
      paddingBottom:6,
      borderLeftWidth:0,
      borderRightWidth:0
    }
};

export  {Table, Row};
