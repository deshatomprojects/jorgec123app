import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  Dimensions
} from 'react-native';
import { AppStyles, Colors, Images } from '../themes';
import {Actions} from 'react-native-router-flux';
import { Button } from './common';
import { APP_NAME } from '../constant';

class RegisterContainer extends Component{

  onLayout(e) {
    const { nativeEvent: { layout: { height } } } = e;
    this.height = height;
    this.forceUpdate();
  }

  render(){
      const {height: heightOfDeviceScreen} = Dimensions.get('window');
    return (
      <ScrollView contentContainerStyle={{minHeight: this.height || heightOfDeviceScreen, padding: 15}}
                  onLayout={this.onLayout.bind(this)}>
        <Button textStyle={styles.grayText} style={styles.backButton} backgroundColor={'transparent'} onPress={()=>Actions.pop()}> {'<Already Have an Account?'}</Button>
        <View style={styles.logoContainer}>
          <Image style={styles.imageStyle} source={Images.logo}></Image>
          <Text style={styles.titleStyle}>{APP_NAME}</Text>
          <Text style={{...AppStyles.h3, ...AppStyles.lightText, }}>{'Let\'s Join With Us'}</Text>
        </View>
        {this.props.children}
      </ScrollView>
    );
  }

};

const styles = StyleSheet.create({
    container: {
        ...AppStyles.screen.container,
        padding: 15,
    },
    backButton:{
      justifyContent:'flex-start'
    },
    logoContainer: {
        ...AppStyles.center,
        padding: 20,
        paddingTop: 0,
    },
    titleStyle: {
        ...AppStyles.h1,
        margin: 5
    },
    imageStyle: {
        width: 90,
        height: 90,
        marginBottom: 10,
    },
    grayText:{
      fontSize: 12,
      color: Colors.grayText,
    }
});

export default RegisterContainer;
