import React from 'react';
import {View, Text} from 'react-native';
import {Colors} from '../themes';

const Holder = ({title, children, style}) => {
    const {containerStyle, titleStyle} = styles;

    return (
        <View style={[containerStyle, style]}>
            <Text style={[titleStyle]}>{title}</Text>
            {children}
        </View>
    );
};
const styles = {
    containerStyle: {
        borderColor: Colors.inputBorderColor,
        borderWidth: 0.5,
        borderRadius: 8,
        padding: 1,
        marginTop: 6,
        marginBottom: 6
    },
    titleStyle: {
        fontSize: 14,
        backgroundColor: Colors.grayBg,
        textAlign: 'center',
        paddingVertical: 8,
        fontWeight: '400'
    }
};

export default Holder;
