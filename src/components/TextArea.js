import React from 'react';
import {TextInput, View, Text} from 'react-native';
import {Colors} from '../themes';
const TextArea = ({
    label,
    value,
    onChangeText,
    placeholder,
    secureTextEntry,
    style
}) => {
    const {inputStyle, labelStyle, containerStyle, iconContainerStyle} = styles;

    return (
        <View style={[containerStyle, style]}>
            <TextInput numberOfLines={4} multiline={true} underlineColorAndroid='rgba(0,0,0,0)' secureTextEntry={secureTextEntry} placeholder={placeholder} autoCorrect={false} style={[
                inputStyle, {
                    color: secureTextEntry
                        ? Colors.primary
                        : Colors.inputColor
                }
            ]} value={value} onChangeText={onChangeText}/>

        </View>
    );
};
const styles = {
    iconContainerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: 46
    },
    inputStyle: {
        flex: 1,
        borderColor: Colors.inputColor,
        fontSize: 14,
        padding: 20
    },
    containerStyle: {
        height: 140,
        flexDirection: 'row',
        margin: 4,
        borderColor: Colors.inputBorderColor,
        borderWidth: 1,
    }
};

export default TextArea;
