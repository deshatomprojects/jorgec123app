import React from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const RightButton = ({iconName, onPress}) => {
    return (
        <TouchableOpacity onPress={onPress} style={styles.navButton}>
            <Icon name={iconName} size={23} color={'white'}/>
        </TouchableOpacity>
    )
}
const styles = {
    navButton: {
        paddingLeft: 12,
        paddingRight: 12,
        height: 30
    }
};

export default RightButton;
