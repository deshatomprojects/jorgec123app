import React from 'react';
import {View} from 'react-native';
import {Colors} from '../../themes/';

const Hr = ({style}) => {

    return (<View style={[styles.container, style]}/>);
};

const styles = {
    container: {
        borderTopColor: 'lightgray',
        borderTopWidth: 1,
        paddingTop: 5,
        paddingBottom: 5
    }
};

export {Hr};
