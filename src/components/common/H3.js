import React from 'react';
import {Text} from 'react-native';
import {Colors} from '../../themes/';

const H3 = ({style, children}) => {

    return (
        <Text style={[styles.container, style]}>{children}</Text>
    );
};

const styles = {
    container: {
        fontSize: 14,
        padding: 6,
        color: 'darkgray'
    }
};

export {H3};
