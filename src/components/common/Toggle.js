import React, {Component} from 'react';
import {Text, TouchableOpacity, View, Image} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import {Colors} from '../../themes';

class Toggle extends Component {

    static propTypes = {
        style: View.propTypes.style
    };

    render() {
        const {
            style,
            children,
            title,
            onPressToggle,
            icon,
            status
        } = this.props;
        return (
            <TouchableOpacity onPress={onPressToggle} style={[styles.conatiner, style]}>
                <Icon name="check-circle" size={28} color={status
                    ? Colors.primary
                    : Colors.gray}/>
            </TouchableOpacity>
        );
    }
}

const styles = {
    cardStyle: {
        height: 138,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        position: 'relative'
    },
    imageStyle: {
        width: 20,
        height: 20
    }
};

export {Toggle};
