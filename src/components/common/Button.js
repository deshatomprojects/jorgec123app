import React from 'react';
import {Text, TouchableOpacity, ActivityIndicator} from 'react-native';
import {Colors} from '../../themes/';

const Button = ({
    onPress,
    children,
    textStyle,
    style,
    withBorder,
    backgroundColor,
    color,
    fontWeight,
    fontSize,
    borderRadius,
    isLoading,
    disabled
}) => {
    const {buttonStyle, txtStyle, activityIndicator} = styles;
    let tempStyle = {
        borderWidth: 0,
        backgroundColor: 'white'
    };
    let tempTextStyle = {
        color: 'black',
        fontWeight: '500'
    };
    if (withBorder) {
        tempStyle = {
            ...tempStyle,
            borderWidth
        }
    }

    if (backgroundColor) {

        tempStyle = {
            ...tempStyle,
            backgroundColor
        }
    }

    if (fontWeight) {
        tempTextStyle = {
            ...tempTextStyle,
            fontWeight
        }
    }
    if (fontSize) {
        tempTextStyle = {
            ...tempTextStyle,
            fontSize
        }
    }

    if (color) {
        tempTextStyle = {
            ...tempTextStyle,
            color
        }
    }
    if (borderRadius) {
        tempStyle = {
            ...tempStyle,
            borderRadius
        }
    }
    if(disabled || isLoading){
      tempStyle = {
          ...tempStyle,
          backgroundColor: Colors.disabledButton,
      }
    }
  return (
    <TouchableOpacity onPress={onPress} style={[buttonStyle,tempStyle, style,]} disabled={isLoading || disabled}>
      {isLoading? <ActivityIndicator color={'white'} style={activityIndicator} styleAttr={"small"} /> : null }

      <Text style={[txtStyle, tempTextStyle,textStyle,]}>
          {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
    txtStyle: {
        padding: 0,
        margin: 0
    },
    activityIndicator: {
        marginRight: 8
    },
    buttonStyle: {
        height: 35,
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 2,
        marginLeft: 4,
        marginRight: 4,

        alignItems: 'center',
        justifyContent: 'center'
    }
};

export {Button};
