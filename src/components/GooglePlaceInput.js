import { Dimensions} from 'react-native';

var {GooglePlacesAutocomplete} = require('react-native-google-places-autocomplete');
import React from 'react';
import {View, Text} from 'react-native';
import {Colors} from '../themes';
import {GOOGLE_API} from '../constant.js';
var {height, width} = Dimensions.get('window');
const GooglePlaceInput = (props) => {

    return (
      <GooglePlacesAutocomplete
        {...props}
      minLength={2}
      autoFocus={false}
      returnKeyType={'default'}
      listViewDisplayed='auto'
      fetchDetails={false}
      debounce={200}
      //  nearbyPlacesAPI=GOOGLE_PLACE_API
       query={{
       // available options: https://developers.google.com/places/web-service/autocomplete
       key: GOOGLE_API,
       language: 'en', // language of the results
       types: '(cities)', // default: 'geocode'
     }}
      styles={{

    textInputContainer: {
      backgroundColor: 'rgba(0,0,0,0)',
      borderTopWidth: 0,
      borderBottomWidth: 0,
    },
    listView:{
      width:width - 18,
      backgroundColor:'rgba(185,185,185,0.03)'
    },
    textInput: {
      marginLeft: 4,
      marginRight: 4,
      marginTop:0,
      height: 38,
      color: '#5d5d5d',
      fontSize: 16,
      borderWidth: 1,
      borderColor: Colors.inputBlueBorderColor,
    },
    predefinedPlacesDescription: {
      color: '#1faadb'
    },
  }}
      currentLocation={false}
      />
    );
};


export default GooglePlaceInput;
