import { SideMenu, List, ListItem } from 'react-native-elements'
import React, {Component} from 'react';
import {View} from 'react-native';
import {Actions,ActionConst, DefaultRenderer} from 'react-native-router-flux';
import {Colors} from '../themes';
import {connect} from 'react-redux';
import {fetchUserProfile, toggleSideMenu} from '../actions';

class SideMenuBar extends Component {
  constructor () {
    super()
    this.state = {
      list: [
        {
          name: 'UnknownContainer',
          subtitle: 'Unknown Container',
          icon: 'question-answer',
          key:'UnknownContainer',
        },
        {
          name: 'Tab View ',
          subtitle: 'Unknown Container Tabs',
          icon: 'chrome-reader-mode',
          key:'UnknownContainerTabs',
        },
        {
          name: 'Card List Tab ',
          subtitle: 'Card List Container',
          icon: 'view-list',
          key:'CardListContainer',
        },
        {
          name: 'Card Details Tab ',
          subtitle: 'Card Details Container',
          icon: 'person-pin-circle',
          key:'CardDetails',
        },
      ]
    }
    this.toggleSideMenu = this.toggleSideMenu.bind(this)
  }
  componentDidMount(){
    this.props.fetchUserProfile();
  }

  onSideMenuChange (isOpen) {
    this.props.toggleSideMenu(isOpen);
  }

  toggleSideMenu () {
    this.props.toggleSideMenu();
  }

  handleNavigate(key){
    this.onSideMenuChange(false);
    switch (key) {
      case 'Profile':
        Actions.Profile();
        break;
      case 'UnknownContainer':
        Actions.UnknownContainer();
        break;
      case 'UnknownContainerTabs':
        Actions.UnknownContainerTabs();
        break;
      case 'CardListContainer':
        Actions.CardListContainer();
        break;
      case 'CardDetails':
        Actions.CardDetails();
        break;
    }
  }

  render () {
    const state = this.props.navigationState;
        const children = state.children;
    const MenuComponent = (
      <View style={{flex: 1, backgroundColor: 'white',borderRightWidth:1,borderRightColor:Colors.inputBorderColor, paddingTop: 50}}>
        <List containerStyle={{marginBottom: 20, borderTopWidth:0}}>
          <ListItem
            underlayColor='rgb(252, 252, 252)'
            roundAvatar
            onPress={() => this.handleNavigate('Profile') }
            key={'profile'}
            rightIcon={{name:'person-pin', }}
            containerStyle={{borderTopWidth:0}}
            title={this.props.profile.first_name?this.props.profile.first_name:'loading'}
            subtitle={this.props.profile.email?this.props.profile.email : 'loading'}
          />
        {
          this.state.list.map((l, i) => (
            <ListItem
              underlayColor='rgb(252, 252, 252)'
              roundAvatar
              // containerStyle={{}}
              onPress={() => this.handleNavigate(l.key) }
              key={i}
              title={l.name}
              leftIcon={{name: l.icon, style:{color:Colors.primary, marginLeft: 15,width: 40,marginRight: 15}}}
              // subtitle={l.subtitle}
            />
          ))
        }
        </List>
      </View>
    )

    return (
      <SideMenu
        isOpen={this.props.app.isOpenSideMenu}
        onChange={this.onSideMenuChange.bind(this)}
        menu={MenuComponent}>
          <DefaultRenderer navigationState={children[0]} onNavigate={this.props.onNavigate} />
      </SideMenu>
    )
  }

}


const mapStateToProps = ({profile, app}) => {
    return {profile, app};
};

export default connect(mapStateToProps, {fetchUserProfile,toggleSideMenu})(SideMenuBar);
