import React from 'react';
import {TextInput, View} from 'react-native';
import {Colors} from '../themes';
const IconInput = ({
    label,
    value,
    onChangeText,
    placeholder,
    secureTextEntry,
    icon,
    style,
    iconContainerStyle,
    inputStyle
}, props) => {
    const {input, containerStyle, iconContainer} = styles;

    return (
        <View style={[containerStyle, style]}>
            <TextInput underlineColorAndroid='rgba(0,0,0,0)' secureTextEntry={secureTextEntry} placeholder={placeholder} autoCorrect={false} style={[
                input, {
                    color: secureTextEntry
                        ? Colors.primary
                        : Colors.inputColor
                }
            , inputStyle]} value={value} onChangeText={onChangeText}/>
            <View style={[iconContainer, iconContainerStyle]}>
                {icon}
            </View>

        </View>
    );
};
const styles = {
    iconContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: 46
    },
    input: {
        flex: 1,
        borderColor: Colors.inputColor,
        fontSize: 14,
        height: 46,
        paddingLeft: 20
    },
    containerStyle: {
        height: 46,
        flexDirection: 'row',
        margin: 4,
        borderColor: Colors.inputBlueBorderColor,
       borderWidth: 1,
       borderRadius: 4,
    }
};

export default IconInput;
