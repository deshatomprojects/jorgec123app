import React from 'react';
import { View, Text} from 'react-native';
import {Colors} from '../themes';

const GroupTotal = ({title, description, style, onPress}) => {
    const {
        boxStyles,
        titleStyle,
        total,
        row,
        field,
        value
    } = styles;

    return (
        <View style={boxStyles}>
            <Text style={titleStyle}>Group Total</Text>
            <Text style={total}>$ 440.00</Text>
            <View style={row}>
                <Text style={field}>Transaction Free ($)</Text>
                <Text style={value}>$ 4.00</Text>

            </View>
        </View>
    );
};
const styles = {
    row: {
        flexDirection: 'row',
        flex: 1,
        marginTop: 2
    },
    field: {
        flex: 1,
        fontSize: 12,
        textAlign: 'center',
        paddingVertical: 8,
        fontWeight: '300'
    },
    value: {
        flex: 1,
        backgroundColor: 'white',
        fontSize: 14,
        color: Colors.secondary,
        textAlign: 'center',
        paddingVertical: 8
    },
    boxStyles: {
        backgroundColor: Colors.grayBg,
        borderColor: Colors.inputBorderColor,
        borderWidth: 0.5,
        borderRadius: 8,
        padding: 1
    },
    titleStyle: {
        textAlign: 'center',
        paddingVertical: 8
    },
    total: {
        fontSize: 18,
        color: Colors.primary,
        textAlign: 'center',
        paddingVertical: 8,
        backgroundColor: 'white'
    }
};

export default GroupTotal;
