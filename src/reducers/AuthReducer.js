import {
    SET_ACCESS_TOKEN,
    USER_LOGOUT,
    UPDATE_AUTH_FIELD,
} from '../actions/types';

const INITIAL_STATE = {
    loading: false,

    access_token: null,
    email: '',
    phone: '',
    old_password: '',
    new_password: '',
    emailErr: false,
    phoneErr: false,
    old_passwordErr: false,
    new_passwordErr: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SET_ACCESS_TOKEN:
            return {...state,
                access_token: action.payload
            };
        case USER_LOGOUT:
            return {...state,
                access_token: null
            };
        case UPDATE_AUTH_FIELD:
            return {...state,
                  [action.payload.prop]: action.payload.value,
                  [action.payload.prop + 'Err']: action.payload.error
            };
        default:
            return state;
    }
};
