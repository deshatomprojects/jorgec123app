import {
  UPDATE_UNKOWN_TAB_FIELD,
  MOCK_REQUEST_TAB_CONTAINER
} from '../actions/types';

const INITIAL_STATE = {
    iconinput1: '',
    iconinput2: '',
    iconinput3: '1',
    iconinput4: '123',
    iconinput5: '',
    iconinput7: '',
    iconinput6: '',

    gpInput1: null,
    gpInput2: null,
    gpInput3: null,
    gpInput4: null,

    d1: new Date(),
    d2: new Date(),
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UPDATE_UNKOWN_TAB_FIELD:
            return {...state,
                [action.payload.key]: action.payload.value,
            };
        case MOCK_REQUEST_TAB_CONTAINER:
            return INITIAL_STATE;
        default:
            return state;
    }
};
