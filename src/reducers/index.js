import {
    combineReducers
} from 'redux';
import {
    USER_LOGOUT,
} from '../actions/types';
import AuthReducer from './AuthReducer';
import LoginReducer from './LoginReducer';
import GroupReducer from './GroupReducer';
import ProfileReducer from './ProfileReducer';
import ReservationReducer from './ReservationReducer';
import RegisterFormReducer from './RegisterFormReducer';
import AppReducer from './AppReducer';
import UnknownReducer from './UnknownReducer';
import UnknownTabReducer from './UnknownTabReducer';
import NewCardListReducer from './NewCardListReducer';
import CardDetailsReducer from './CardDetailsReducer';

const appReducer = combineReducers({
    login: LoginReducer,
    auth: AuthReducer,
    reg: RegisterFormReducer,
    profile: ProfileReducer,
    group: GroupReducer,
    reservation: ReservationReducer,
    app: AppReducer,
    unknown: UnknownReducer,
    unknownTab: UnknownTabReducer,
    newCardList: NewCardListReducer,
    cardDetails: CardDetailsReducer,
})

const rootReducer = (state, action) => {
    if (action.type === USER_LOGOUT) {
        state = undefined
    }

    return appReducer(state, action)
}

export default rootReducer;
