

const INITIAL_STATE = [
  {
    from: 'Colombo',
    to: 'Bejin',
    list: ["Colombo", "Tokio", "China"],
    boxList: [
      { title: "ChZ", time: "7.30PM" },
      { title: "CRZ", time: "8.30PM" },
      { title: "ZhQ", time: "9.30PM" },
      { title: "TEI", time: "1.30PM", icon: "md-time" }
    ]
  },
  {
    from: 'NY',
    to: 'Amsterdam',
    list: ["NY", "London", "Amsterdam"],
    boxList: [
      { title: "NY", time: "7.30PM" },
      { title: "RTX", time: "2.30PM" },
      { title: "ZhQ", time: "3.30PM" },
      { title: "RKL", time: "1.40PM", icon: "md-time" }
    ]
  },
  {
    from: 'Sydny',
    to: 'Colombo',
    list: ["Sydny", "Male", "Colombo"],
    boxList: [
      { title: "ChZ", time: "7.30PM" },
      { title: "CRZ", time: "8.30PM" },
      { title: "ZhQ", time: "9.30PM" },
      { title: "TEI", time: "1.30PM", icon: "md-time" }
    ]
  },
  {
    from: 'Colombo',
    to: 'Bejin',
    list: ["Colombo", "Tokio", "China"],
    boxList: [
      { title: "ChZ", time: "7.30PM" },
      { title: "CRZ", time: "8.30PM" },
      { title: "ZhQ", time: "9.30PM" },
      { title: "TEI", time: "1.30PM", icon: "md-time" }
    ]
  },
  {
    from: 'NY',
    to: 'Amsterdam',
    list: ["NY", "London", "Amsterdam"],
    boxList: [
      { title: "NY", time: "7.30PM" },
      { title: "RTX", time: "2.30PM" },
      { title: "ZhQ", time: "3.30PM" },
      { title: "RKL", time: "1.40PM", icon: "md-time" }
    ]
  },
]

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        default:
            return state;
    }
};
