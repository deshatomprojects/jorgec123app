import {
    USER_LOGIN,
    LOGGING_SUCCESS,
    LOGGING_FAILED,
    LOGIN_FORM_UPDATE,
} from '../actions/types';

const INITIAL_STATE = {
    loading: false,
    username: '',
    usernameErr: true,
    password: '',
    passwordErr: true,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case USER_LOGIN:
            return {...state,
                loading: true,
            };
        case LOGGING_SUCCESS:
            return {...state,
                loading: false,
            };
        case LOGGING_FAILED:
            return {...state,
                loading: false,
            };
        case LOGIN_FORM_UPDATE:
            return {...state,
                [action.payload.prop]: action.payload.value,
                [action.payload.prop + 'Err']: action.payload.error
            };
        default:
            return state;
    }
};
