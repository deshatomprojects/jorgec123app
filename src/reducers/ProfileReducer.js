import {
    UPDATE_USER_PROFILE_FIELD,
    FETCHED_PROFILE,
    UPDATE_MAIL_FIELD
} from '../actions/types';

const INITIAL_STATE = {
    first_name: '',
    last_name: '',
    middle_name: '',
    street: '',
    city: '',
    state: '',
    zip: '',
    id: null,

    first_nameErr: false,
    last_nameErr: false,
    mailing: 'same',

};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UPDATE_USER_PROFILE_FIELD:
            return {...state,
                [action.payload.prop]: action.payload.value,
                [action.payload.prop + 'Err']: action.payload.error
            };
        case UPDATE_MAIL_FIELD:
            return {...state,
                mailing: {
                    ...state.mailing,
                    [action.payload.prop]: action.payload.value
                }
            };
        case FETCHED_PROFILE:
            const newState = action.payload;
            return {...state,
                ...newState
            }
        default:
            return state;
    }
};
