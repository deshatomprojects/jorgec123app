import {
    FETCHED_GROUPS,
    FETCH_USERS_BY_GROUP_ID,
    UPDATE_USER_SHARE,
    UPDATE_RESERVATION_FIELD,
    UPDATE_USER_SHARE_ERROR,
    FETCHED_NEW_GROUPS
} from '../actions/types';

const INIT_USERS = [
  {userId:0, name: 'user 1', share: 10, price:10},
  {userId:1, name: 'user 2', share: 40, price:10},
  {userId:2, name: 'user 3', share: 50, price:50},
];
const INIT_RESERVATION = {
  reservation_1: {reservation:1, dinner: 'Dinner 1',reservation: 'Reservation 1'},
  reservation_2:{ reservation:2, dinner: 'Dinner 2',reservation: 'Reservation 2'},
  reservation_3 :{reservation:3, dinner: 'Dinner 3',reservation: 'Reservation 3'},
}

const INITIAL_STATE = {
    list: [],
  users: INIT_USERS,
  reservations: INIT_RESERVATION,
  userShareError: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCHED_GROUPS:
            return {...state, list: action.payload};
        case FETCHED_NEW_GROUPS:
            if( action.payload &&  action.payload.length>0){
              var newList = state.list;
              console.log('newList',newList)
              console.log('newList.push()',newList.push(action.payload[0]))
              console.log('newList',newList)
return {...state, list: newList};
            }else{
return {...state};
            }

        case FETCH_USERS_BY_GROUP_ID:
            return state;
        case UPDATE_USER_SHARE_ERROR:
            return {...state, userShareError: action.payload};
        case UPDATE_USER_SHARE:
            var users = state.users;
            users[action.payload.userId].share = action.payload.value;
            return {...state, users};
        default:
            return state;
    }
};
