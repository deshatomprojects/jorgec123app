import {
  UPDATE_RESERVATION_FIELD
} from '../actions/types';
const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case UPDATE_RESERVATION_FIELD:
      return { ...state,
        [action.payload.prop]: action.payload.value,
        [action.payload.prop + 'Err']: action.payload.error
      };
    default:
      return state;
  }
};
