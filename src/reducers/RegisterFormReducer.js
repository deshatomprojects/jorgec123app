import {
    REGISTER_FORM_UPDATE,
    REGISTERING_USER,
    REGISTER_SUCCESS,
    REGISTER_FAILED
} from '../actions/types';

const INITIAL_STATE = {
    loading: false,
    username: '',
    first: '',
    middle: '',
    last: '',
    email: '',
    phone: '',
    password: '',
    passwordVerify: '',

    usernameErr: true,
    firstErr: true,
    middleErr: false,
    lastErr: true,
    emailErr: true,
    phoneErr: true,
    passwordErr: true,
    passwordVerifyErr: true,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case REGISTER_FORM_UPDATE:
            return {...state,
                [action.payload.prop]: action.payload.value,
                [action.payload.prop + 'Err']: action.payload.error
            };
        case REGISTERING_USER:
            return {...state,
                loading: true
            };
        case REGISTER_SUCCESS:
            return {...state,
                loading: false
            };
        case REGISTER_FAILED:
            return {...state,
                loading: false
            };
        default:
            return state;
    }
};
