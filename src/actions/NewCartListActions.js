fetchMoreCards

'use strict';

import axios from 'axios';
import async from 'async';
import _ from 'lodash';
import {FETCH_MORE_CARDS} from './types';

function randomlyGetCardDetails_demo_purpose_only() {
  var fake_list = [  {
      from: 'Colombo',
      to: 'Bejin',
      list: ["Colombo", "Tokio", "China"],
      boxList: [
        { title: "ChZ", time: "7.30PM" },
        { title: "CRZ", time: "8.30PM" },
        { title: "ZhQ", time: "9.30PM" },
        { title: "TEI", time: "1.30PM", icon: "md-time" }
      ]
    },
    {
      from: 'NY',
      to: 'Amsterdam',
      list: ["NY", "London", "Amsterdam"],
      boxList: [
        { title: "NY", time: "7.30PM" },
        { title: "RTX", time: "2.30PM" },
        { title: "ZhQ", time: "3.30PM" },
        { title: "RKL", time: "1.40PM", icon: "md-time" }
      ]
    },
    {
      from: 'Sydny',
      to: 'Colombo',
      list: ["Sydny", "Male", "Colombo"],
      boxList: [
        { title: "ChZ", time: "7.30PM" },
        { title: "CRZ", time: "8.30PM" },
        { title: "ZhQ", time: "9.30PM" },
        { title: "TEI", time: "1.30PM", icon: "md-time" }
      ]
    },
    {
      from: 'Colombo',
      to: 'Bejin',
      list: ["Colombo", "Tokio", "China"],
      boxList: [
        { title: "ChZ", time: "7.30PM" },
        { title: "CRZ", time: "8.30PM" },
        { title: "ZhQ", time: "9.30PM" },
        { title: "TEI", time: "1.30PM", icon: "md-time" }
      ]
    },
    {
      from: 'NY',
      to: 'Amsterdam',
      list: ["NY", "London", "Amsterdam"],
      boxList: [
        { title: "NY", time: "7.30PM" },
        { title: "RTX", time: "2.30PM" },
        { title: "ZhQ", time: "3.30PM" },
        { title: "RKL", time: "1.40PM", icon: "md-time" }
      ]
    }];

    return fake_list[_.random(0, 4)];
}
/**
 * Checks wheather User has typed something wrong.
 * @return {Boolean}
 */
export const fetchMoreCards = () => {
  return (dispatch, getState) => {
    async.map([
      'get_request_1',
      'get_request_2',
      'get_request_3',
      'get_request_4',
      'get_request_5',
      'get_request_6',
      'get_request_7'
    ], function(singleRequest, next) {

      // demo purpose start
      return next(null, randomlyGetCardDetails_demo_purpose_only());
      // demo purpose end

      /** // -------------------------------------------------------------------
          //  This is how you should fetch data
          // -------------------------------------------------------------------
                axios.get(`http://example.com/morecards/${siyngleRequest}`).then(response => {
                  return next(null, response.data)
                }).catch((error) => {
                  return next(error)
                });
         // -------------------------------------------------------------------
      **/

    }, function(err, results) {
      /** // -------------------------------------------------------------------
          //  This is how you should manage errors
          // -------------------------------------------------------------------
                if(err){
                  return showErrors();
                }
         // -------------------------------------------------------------------
      **/

      return dispatch({
        type: FETCH_MORE_CARDS,
        payload: results
      });

    });
  }
}
