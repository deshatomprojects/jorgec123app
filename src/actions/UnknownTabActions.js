'use strict';

import axios from 'axios';
import _ from 'lodash';
import {
  UPDATE_UNKOWN_TAB_FIELD,
  MOCK_REQUEST_TAB_CONTAINER
} from './types';
import {
    Actions
} from 'react-native-router-flux';

export const updateUnknownTabField = (key, value) => {
    return {
        type: UPDATE_UNKOWN_TAB_FIELD,
        payload: {
            key,
            value
        }
    };
}

export const mockRequestTabContainer = (object) => {
    alert(JSON.stringify(object));
    Actions.refresh();
    return {
        type: MOCK_REQUEST_TAB_CONTAINER,
    };
}
