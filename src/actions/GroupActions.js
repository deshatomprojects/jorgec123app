'use strict';

import axios from 'axios';
import {
    CREATE_GROUP,
    FETCHED_GROUPS,
    FETCH_USERS_BY_GROUP_ID,
    UPDATE_USER_SHARE,
    UPDATE_RESERVATION_FIELD,
    UPDATE_USER_SHARE_ERROR,
    FETCHED_NEW_GROUPS
} from './types';
import {
    Actions
} from 'react-native-router-flux';

/**
 * Create Group
 * @param  {String} name [descirption of the group]
 * @attribute {access_token} stored access token
 */
export const createGroup = (name) => {

    return (dispatch, getState) => {
        const {
            access_token
        } = getState().auth;
        axios.post(`/groups/create`, {
                name,
                access_token
            }).then(response => {
                dispatch(fetchGroups());
                Actions.pop();
            })
            .catch((error) => {
                alert(error)
            });
    };
}

/**
 * Retrive list of groups
 * @attribute {access_token} stored access token
 *
 */
export const fetchGroups = () => {

    return (dispatch, getState) => {
        const {
            access_token
        } = getState().auth;

        axios.get(`/groups/list?access_token=${access_token}`)
              .then(response => {
                dispatch({
                    type: FETCHED_GROUPS,
                    payload: response.data.data
                })

            })
            .catch((error) => {
                alert(error)
            });
    };
}


export const createNewGroupAndFetch = (name) => {
  console.log('name',name)
      return (dispatch, getState) => {
          const {
              access_token
          } = getState().auth;
          axios.post(`/groups/create`, {
                  name,
                  access_token
              }).then(response => {
                  dispatch(fetchGroups());

              })
              .catch((error) => {
                  alert(error)
              });
      };
  }


/**
 * Fetch Users by Group Id
 * @param  {String} groupId
 */
export const fetchUsersByGroupId = (groupId) => {
    return (dispatch, getState) => {


       dispatch({type: FETCH_USERS_BY_GROUP_ID})
    };
}

/**
 * updateUserShare
 * @param  {String} userId
 * @param  {Integer} int
 */
export const updateUserShare = (userId, value=0) => {

    return (dispatch, getState) => {

      dispatch({type: UPDATE_USER_SHARE, payload:{value, userId}})
      const { users} = getState().group;
      let total = 0;
      for (var i = 0; i < users.length; i++) {
        total += parseInt(users[i].share);
      }
      if(total != 100 ){
        dispatch({type: UPDATE_USER_SHARE_ERROR, payload:'Total share must be 100'})
      }else{
        dispatch({type: UPDATE_USER_SHARE_ERROR, payload:null})
      }

    };
}

export const updateReservation = ({
    prop,
    value,
    required,
    validate
}) => {
    let error = false;
    if (!value || value === '') {
        if (required) {
            error = true;
        }
    }

    return {
        type: UPDATE_RESERVATION_FIELD,
        payload: {
            prop,
            value,
            error
        }
    };
}

/**
 * Join with a Group
 * @param  {String} name [descirption of the group]
 * @attribute {access_token} stored access token
 */
export const requestToJoinGroup = (groupId) => {

    return (dispatch, getState) => {
        const {
            access_token
        } = getState().auth;
        axios.post(`/groups/request/join`, {
                groupId,
                access_token
            }).then(response => {
                alert('Joined..');
            })
            .catch((error) => {
                alert(error)
            });
    };
}
