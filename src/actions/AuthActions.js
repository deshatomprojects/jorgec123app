'use strict';
import axios from 'axios';
import qs from 'qs';
import {
    USER_LOGIN,
    LOGGING_SUCCESS,
    LOGGING_FAILED,
    LOGIN_FORM_UPDATE,
    USER_LOGOUT,
    SET_ACCESS_TOKEN,
    UPDATE_AUTH_FIELD
} from './types';
import {
    Actions
} from 'react-native-router-flux';
import {fetchUserProfile} from './ProfileActions';
/**
 * Update login form field
 * @param  {String} prop name ex: first_name
 * @param  {String} value     value of the field
 * @param  {Boolean} required  required field or not
 * @param  {String} validate   Regex Strings
 */
export const updateLoginForm = ({
    prop,
    value,
    required,
    validate
}) => {
    let error = false;
    if (!value || value === '') {
      if (required) {
          error = true;
      }
    }

    if (validate) {
        if (validate === 'email') {
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(value)) {
                error = true;
            }
        }
    }

    return {
        type: LOGIN_FORM_UPDATE,
        payload: {
            prop,
            value,
            error
        }
    };
}

/**
 * logout
 * TODO: this should have a api call to destroy the token key
 */
export const logout = () => {

    return (dispatch) => {
       Actions.auth({type: "reset"});
        dispatch({
            type: USER_LOGOUT
        });

    };

}

/**
 * Calling to userLogin api
 * @param  {String} username
 * @param  {String} password
 */
export const loginUser = (username, password) => {

    return (dispatch) => {
        dispatch({
            type: USER_LOGIN
        });
        axios.post(`/accounts/login`, {
                username,
                password
            }).then(response => {
              console.log('login sucess', response)

                dispatch({
                    type: LOGGING_SUCCESS,
                });

                //axios.defaults.headers.common['Authorization'] = response.data.data.access_token;
                dispatch({
                    type: SET_ACCESS_TOKEN,
                    payload: response.data.data.access_token,
                });
                dispatch(fetchUserProfile());
                Actions.main();
            })
            .catch((error) => {

                //axios.defaults.headers.common['Authorization'] = null;
                alert(error);
                dispatch({
                    type: LOGGING_FAILED
                });
            });
    };
}


/**
 * Update Auth Field
 *
 * @param  {String} prop name ex: first_name
 * @param  {String} value     value of the field
 * @param  {Boolean} required  required field or not
 * @param  {String} validate   Regex Strings
 */
export const updateAuthField = ({
    prop,
    value,
    required,
    validate
}) => {
    let error = false;
    if (!value || value === '') {
      if (required) {
          error = true;
      }
    }

    if (validate) {
        if (validate === 'email') {
            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(value)) {
                error = true;
            }
        }
    }

    return {
        type: UPDATE_AUTH_FIELD,
        payload: {
            prop,
            value,
            error
        }
    };
}

/**
 * Update Email
 * @param  {String} email New email address
 */
export const updateEmail = (email) => {

    return (dispatch, getState) => {
      const {
          access_token
      } = getState().auth;
        axios.put(`/users/email`, {
                email,
                access_token
            }).then(response => {
              alert('sucessfully updated');
              dispatch(updateAuthField({prop:'email', value: '', required:true}))
            })
            .catch((error) => {
                alert(error);
            });
    };
}


/**
 * Update Phone
 * @param  {String} phone_number New phone_number address
 */
export const updatePhone = (phone_number) => {
    return (dispatch, getState) => {
      const {
          access_token
      } = getState().auth;
        axios.put(`/users/phone_number`, {
                phone_number,
                access_token
            }).then(response => {
              alert('sucessfully updated');
              dispatch(updateAuthField({prop:'phone', value: '', required:true}))
            })
            .catch((error) => {
                alert(error);
            });
    };
}


/**
 * Update Phone
 * @param  {String} old_password  old_password address
 * @param  {String} new_password  new_password address
 */
export const updatePassword = (old_password, new_password) => {

    return (dispatch, getState) => {
      const {
          access_token
      } = getState().auth;
        axios.put(`/users/password`, {
                old_password,
                new_password,
                access_token
            }).then(response => {
              alert('sucessfully updated');
              dispatch(updateAuthField({prop:'old_password', value: '', required:true}));
              dispatch(updateAuthField({prop:'new_password', value: '', required:true}));

            })
            .catch((error) => {
                alert(error);
            });
    };
}
