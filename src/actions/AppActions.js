import {
    TOGGLE_SIDE_MENU
} from './types';
import {
    Actions
} from 'react-native-router-flux';

/**
 * Toggle Side Menu
 * @param  {Boolean} isOpen is optional
 */
export const toggleSideMenu = (isOpen) => {
    return (dispatch, getState) => {
        let { app } = getState();

        if(isOpen != undefined){
          return dispatch({
              type: TOGGLE_SIDE_MENU,
              payload: isOpen
          });

        } else{
          return dispatch({
              type: TOGGLE_SIDE_MENU,
              payload: !app.isOpenSideMenu
          });
        }


        return hasTypeError;
    }
}
