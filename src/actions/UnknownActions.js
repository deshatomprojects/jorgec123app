'use strict';

import axios from 'axios';
import _ from 'lodash';
import {
  UPDATE_UNKOWN_FIELD,
  MOCK_REQUEST_CONTAINER
} from './types';
import {
    Actions
} from 'react-native-router-flux';

export const updateUnknownField = (key, value) => {

    return {
        type: UPDATE_UNKOWN_FIELD,
        payload: {
            key,
            value
        }
    };
}

export const mockRequestContainer = (object) => {
    alert(JSON.stringify(object));
    Actions.refresh();
    return {
        type: MOCK_REQUEST_CONTAINER,
    };
}
