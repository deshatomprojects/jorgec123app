'use strict';

import axios from 'axios';
import _ from 'lodash';
import {
    UPDATE_USER_PROFILE_FIELD,
    FETCHED_PROFILE,
    UPDATE_MAIL_FIELD
} from './types';
import {
    Actions
} from 'react-native-router-flux';

/**
 * Update Profile State
 * @param  {String} prop name ex: first_name
 * @param  {String} value     value of the field
 * @param  {Boolean} required  required field or not
 * @param  {String} validate   Regex Strings
 */
export const updateProfileField = ({
    prop,
    value,
    required,
    validate
}) => {
    let error = false;
    if (!value || value === '') {
        if (required) {
            error = true;
        }
    }

    return {
        type: UPDATE_USER_PROFILE_FIELD,
        payload: {
            prop,
            value,
            error
        }
    };
}

/**
* Update Mail Fileds
*
* @param  {String} prop name ex: first_name
* @param  {String} value     value of the field
* @param  {Boolean} required  required field or not
* @param  {String} validate   Regex Strings
 */
export const updateMailField = ({
    prop,
    value,
    required,
    validate
}) => {
    let error = false;
    if (!value || value === '') {
        if (required) {
            error = true;
        }
    }

    return {
        type: UPDATE_MAIL_FIELD,
        payload: {
            prop,
            value,
            error
        }
    };
}

/**
 * Checks wheather User has typed something wrong.
 * @return {Boolean}
 */
export const hasProfileTypeError = () => {
    return (dispatch, getState) => {
        let hasTypeError = false;
        let {
            profile
        } = getState();
        _.mapKeys(profile, function(value, key) {
            if (_.endsWith(key, 'Err')) {
                if (value) {
                    hasTypeError = true;
                }
            }
        });

        return hasTypeError;
    }
}

/**
 * Fetch User Profile Information using token
 */
export const fetchUserProfile = () => {
    return (dispatch, getState) => {
        const {
            access_token
        } = getState().auth;
        console.log(access_token)
        axios.get(`/users/profile`, {
                params: {
                    access_token
                }
            })
            .then(function(response) {
                if (response.data.data) {
                    if (response.data.data.length > 0) {
                        dispatch({
                          type:FETCHED_PROFILE,
                          payload: response.data.data[0]
                        })
                    } else {
                        alert('No Data');
                    }
                } else {
                    alert('No Data');

                }
            })
            .catch(function(error) {
                alert(error.response.data.message);
            });
    }
}


/**
 * Update user profile using token
 */
export const updateProfile = () => {
    return (dispatch, getState) => {
        const {
            access_token
        } = getState().auth;
        const {
          first_name,
          last_name,
          middle_name,
          street,
          city,
          state,
          zip,
          mailing
        } = getState().profile;
        console.log(access_token)
        axios.put(`/users/profile`, {
                    access_token,
                    first_name,
                    last_name,
                    middle_name,
                    street,
                    city,
                    state,
                    zip,
                    mailing
            })
            .then(function(response) {
                if (response.data.data) {
                    console.log(response);
                    alert('Updated Profile');
                    dispatch(fetchUserProfile());
                } else {
                    alert('No Data');

                }
            })
            .catch(function(error) {
                alert(error.response.data.message);
            });
    }
}
