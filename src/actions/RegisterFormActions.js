'use strict';

import axios from 'axios';
import _ from 'lodash';
import {
    REGISTER_FORM_UPDATE,
    REGISTERING_USER,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    SET_ACCESS_TOKEN
} from './types';
import {
    Actions
} from 'react-native-router-flux';

/**
 * Update register form field
 * @param  {String} prop name ex: first_name
 * @param  {String} value     value of the field
 * @param  {Boolean} required  required field or not
 * @param  {String} validate   Regex Strings
 */
export const updateRegisterForm = ({
    prop,
    value,
    required,
    validate
}) => {
    let error = false;
    if (!value || value === '') {
        error = true;
    }

    if (validate) {
        if (validate === 'email') {
            let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(value)) {
                error = true;
            }
        }
    }

    return {
        type: REGISTER_FORM_UPDATE,
        payload: {
            prop,
            value,
            error
        }
    };
}

/**
 * Checks wheather User has typed something wrong.
 * @return {Boolean}
 */
export const hasTypeError = () => {

    return (dispatch, getState) => {
        let hasTypeError = false;
        let {
            reg
        } = getState();
        _.mapKeys(reg, function(value, key) {
            if (_.endsWith(key, 'Err')) {
                if (value) {
                    hasTypeError = true;
                }
            }
        });

        return hasTypeError;
    }
}

/**
 * Register user - calling api
 */
export const registerUser = () => {

    return (dispatch, getState) => {
        const {
            username,
            password,
            email,
            first,
            middle,
            last,
            phone
        } = getState().reg;
        dispatch({
            type: REGISTERING_USER
        });
        axios.post(`/accounts/signup`, {
                username,
                password,
                email,
                first_name: first,
                middle_name: middle,
                last_name: last,
                phone_number: phone,
                terms_and_conditions: true
            }).then(response => {
                console.log(response);
                dispatch({
                    type: REGISTER_SUCCESS,
                });
                //axios.defaults.headers.common['Authorization'] = response.data.data.access_token;
                dispatch({
                    type: SET_ACCESS_TOKEN,
                    payload: response.data.data.access_token,
                });
                Actions.main();
            })
            .catch((error) => {
                console.log(error)
                axios.defaults.headers.common['Authorization'] = null;
                alert(error.response.data.message);
                dispatch({
                    type: REGISTER_FAILED
                });
            });
    };
}
