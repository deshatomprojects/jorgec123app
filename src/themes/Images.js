// @flow

// leave off @2x/@3x
const images = {
    logo: require('../images/logo.jpg'),
    user: require('../images/user.png'),
    lock: require('../images/lock.png'),
    // background: require('../Images/BG.png')
}

export default images
