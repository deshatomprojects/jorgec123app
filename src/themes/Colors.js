// @flow

const colors = {
    primary: '#07c748',
    primaryDark: '#07c748',
    secondary: '#3779f9',
    gray: '#9ba5b9',

    navBarColor: '#3779f9',
    inputColor: '#444444',
    inputPlaceholderColor: '#b4b4b4',
    inputBlueBorderColor: 'rgba(55, 121, 249, 0.31)',
    inputBorderColor: '#dcdcdc',
    grayBg: '#FBFBF6',

    grayText: '#9ba5b9',
    disabledButton: '#b5becf',
    danger:'#d80000',

    bg: 'white',

}

export default colors
