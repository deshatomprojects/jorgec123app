// @flow
import Colors from './Colors'

// This file is for a reusable grouping of Theme items.
// Similar to an XML fragment layout in Android

const AppStyles = {
    screen: {
        container: {
            flex: 1,
            backgroundColor: Colors.bg
        },
        backgroundImage: {
            position: 'absolute',
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        },
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    h1: {
        fontSize: 22,
        fontWeight: '600',
    },
    h2: {
        fontSize: 18,
        fontWeight: '600',
    },
    h3: {
        fontSize: 16,
        fontWeight: '500',
    },
    lightText: {
        color: Colors.grayText
    },
    thin: {
        fontWeight: '300'
    }
}

export default AppStyles;
